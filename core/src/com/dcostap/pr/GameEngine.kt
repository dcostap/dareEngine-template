package com.dcostap.pr

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.dcostap.engine.Assets
import com.dcostap.engine.Engine
import com.dcostap.engine.utils.font_loaders.smart_font_generator.SmartFontGenerator
import com.dcostap.pr.game.GameScreen

/**
 * Created by Darius on 26/03/2018.
 */
class GameEngine : Engine() {
    companion object {
        /** Changes the static variables on Engine class. Call first when the app starts */
        fun initStaticVars() {
            Engine.PPM_WIDTH = 31
            Engine.PPM_HEIGHT = 31

            Engine.BASE_WIDTH = 940
            Engine.BASE_HEIGHT = 680

            Engine.DEBUG = true
            Engine.DEBUG_COLLISION_TREE_CELLS = false
            Engine.DEBUG_COLLISION_TREE_UPDATES = false

            Engine.DEBUG_STAGE_UI = true
            Engine.DEBUG_STAGE_UI_ENTITY_INFO = true

            Engine.DEBUG_STAGE_UI_ENTITY_POPUP_INFO = true

            Engine.DEBUG_ENTITIES = true

            Engine.DEBUG_MAP_CELLS = true

            SmartFontGenerator.fontVersion = "1.0"
            SmartFontGenerator.alwaysRegenerateFonts = false

            SmartFontGenerator.desktopDebugGenerateFontsOnHomeFolder = true
        }
    }

    override fun getAssets(): Assets {
        return gameAssets
    }

    lateinit var gameAssets: GameAssets
        private set

    override fun create() {
        super.create()

        this.setScreen(LoadingScreen())

        gameAssets = GameAssets()
        gameAssets.initAssetLoading()
    }

    private fun finishedLoading() {
        pixelTexture = gameAssets.findRegion("pixel")
        this.setScreen(GameScreen(this))
    }

    override fun render() {
        super.render()

        // wait for assets to load while in loading screen
        if (getScreen() is LoadingScreen && gameAssets.processAssetLoading()) {
            finishedLoading()
        }

        // debug purposes
        if (Gdx.input.isKeyJustPressed(Input.Keys.R)) {
            getScreen()?.dispose()
            this.setScreen(GameScreen(this))
        }
    }
}