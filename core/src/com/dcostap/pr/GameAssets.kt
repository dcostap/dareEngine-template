package com.dcostap.pr

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.Window
import com.dcostap.engine.Assets
import com.dcostap.engine.Engine
import com.dcostap.engine.map.map_loading.JsonMapLoader
import com.dcostap.engine.utils.font_loaders.BitmapFontLoader
import com.dcostap.engine.utils.font_loaders.FontSetNormal
import com.dcostap.engine.utils.font_loaders.FontSetSingle
import com.dcostap.engine.utils.font_loaders.FreeTypeFontLoader

class GameAssets : Assets() {
    val fontDefault = FontSetNormal()
    val fontPixel = FontSetSingle()
    val fontPixelOutline = FontSetSingle()

    lateinit var skin: Skin
        private set

    val map = JsonMapLoader("map", "maps", "maps", "maps")

    override fun setupFontsToBeLoaded() {
        val ttpFontFolder = "fonts/true_type"
        val bitmapFontFolder = "fonts/bitmap"

        // create the font loaders
        val resolutionBaseWidth = Engine.BASE_WIDTH
        addFontLoader(FreeTypeFontLoader(ttpFontFolder, "Roboto-Regular.ttf",
                fontDefault, "font1", useDensityFactor = false, useResolutionFactor = true, baseResolutionWidth = resolutionBaseWidth))
        addFontLoader(BitmapFontLoader("fonts/bitmap", "darenciusPixel.fnt", fontPixel))
        addFontLoader(FreeTypeFontLoader(ttpFontFolder, "darenciusPixel.ttf", fontPixelOutline,
                "font2", FreeTypeFontGenerator.FreeTypeFontParameter().also {
            it.size = 5; it.shadowColor = Color.BLACK; it.shadowOffsetX = 1; it.shadowOffsetY = 1
        }, false, false))
    }

    override fun getDebugFont(): BitmapFont {
        return fontPixel.font
    }

    override fun justFinishedAssetLoading() {
        skin = loadSkin("skins", "skin")

        // scale skin's 9patches
        val scalingFactor1 = Engine.getResolutionFactor(900f)
        val scalingFactor2 = Engine.getResolutionFactor(600f)

        for (name in arrayOf("button1", "button1_2", "button1_disabled")) {
            scaleNinePatchDrawable(skin, name, scalingFactor1)
        }

        for (name in arrayOf("button2", "button3", "transparencyBox", "transparencyButton")) {
            scaleNinePatchDrawable(skin, name, scalingFactor2)
        }

        // pixelated scaling on the font
        fontPixel.font.region.texture.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest)

        // add styles to the skin
        skin.add("default", Window.WindowStyle(fontDefault.font_small, Color.BLACK, skin.getDrawable("button3")))
    }
}