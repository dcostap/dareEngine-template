package com.dcostap.pr.game

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.viewport.ExtendViewport
import com.badlogic.gdx.utils.viewport.Viewport
import com.dcostap.engine.Engine
import com.dcostap.engine.map.EntityTiledMap
import com.dcostap.engine.ui.utils.ExtLabel
import com.dcostap.engine.utils.GameDrawer
import com.dcostap.engine.utils.Utils
import com.dcostap.engine.utils.moveToBottomLeft
import com.dcostap.engine.utils.screens.BaseScreenWithUI
import com.dcostap.engine.utils.use
import com.dcostap.pr.GameEngine
import com.kotcrab.vis.ui.VisUI

/**
 * Created by Darius on 28/12/2017
 */
class GameScreen(val game: GameEngine) : BaseScreenWithUI(game, game.gameAssets.skin) {
    init {
        ExtLabel.defaultFont = VisUI.getSkin().getFont("default-font")
        ExtLabel.defaultColor = Color.WHITE
    }

    val map: EntityTiledMap = EntityTiledMap(game, worldViewport, inputController, stageInputController, false)

    val gameUI = GameUI(this)

    init {
        map.initMap(30, 30, 8)

        camera.moveToBottomLeft(0f, 0f)
        gameUI.createDebugUI()
    }

    override fun createViewport(): Viewport {
        return ExtendViewport(Engine.BASE_WIDTH / Engine.PPM_WIDTH.toFloat(),
                Engine.BASE_HEIGHT / Engine.PPM_HEIGHT.toFloat(), camera.also {
            it.zoom = 1 / 2f
        })
    }

    override fun createStage(): Stage {
        //return Stage(ScreenViewport())

        val uiZoom = 2f
        return Stage(ExtendViewport(Engine.BASE_WIDTH.toFloat() / uiZoom,
                Engine.BASE_HEIGHT.toFloat() / uiZoom, OrthographicCamera()))
    }

    override fun update(delta: Float) {
        super.update(delta)
        gameUI.update(delta)
        map.update(delta)

        // CAMERA MOVEMENT //
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            camera.translate(-0.1f, 0f, 0f)
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            camera.translate(0.1f, 0f, 0f)
        }
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            camera.translate(0f, -0.1f, 0f)
        }
        if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            camera.translate(0f, 0.1f, 0f)
        }
    }

    override fun draw(gameDrawer: GameDrawer, delta: Float) {
        Utils.clearScreen()

        // update camera & viewport
        worldViewport.apply()

        // start the batch
        game.batch.projectionMatrix = camera.combined

        game.batch.use {
            map.draw(gameDrawer, delta)
        }

        game.drawDebugScreen(delta)
        uiController.draw(gameDrawer, delta) // updates & draws the Stage
    }

    override fun dispose() {
        super.dispose()

        Engine.debugStage.clear()
    }
}
