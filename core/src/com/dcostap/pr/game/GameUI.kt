package com.dcostap.pr.game

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.dcostap.engine.Engine
import com.dcostap.engine.ui.utils.ExtLabel
import com.dcostap.engine.ui.utils.ExtTable
import com.dcostap.engine.ui.utils.ResizableActorTable
import com.dcostap.engine.utils.Updatable
import com.dcostap.engine.utils.Utils
import com.dcostap.engine.utils.addChangeListener
import com.kotcrab.vis.ui.VisUI
import com.kotcrab.vis.ui.widget.CollapsibleWidget
import com.kotcrab.vis.ui.widget.VisCheckBox
import com.kotcrab.vis.ui.widget.VisTextButton
import com.kotcrab.vis.ui.widget.VisWindow

/** Created by Darius on 11-Nov-18. */
class GameUI(val gs: GameScreen) : Updatable {
    private val visUIskin = VisUI.getSkin()
    private val gameAssets =  gs.game.gameAssets
    private val gameSkin = gs.game.gameAssets.skin
    private val uiController = gs.uiController

    override fun update(delta: Float) {

    }

    private val debugTopLeft = ResizableActorTable()

    fun createDebugUI() {
        debugTopLeft.clear()
        debugTopLeft.remove()
        Engine.debugStage.addActor(debugTopLeft.also {
            //            it.setPosition(0f, 0f)
            it.top().left()
            it.setFillParent(true)

            it.add(VisWindow("").also {
                //                it.isMovable = true

                it.defaults().top()

                it.background = Utils.solidColorDrawable(0f, 0f, 0f, 0.56f)
                it.pad(3f)

                it.add(ExtLabel().also {
                    it.textUpdateFunction = {"fps: ${Gdx.graphics.framesPerSecond}  rc: ${gs.game.gameBatchRenderCalls}" +
                            "\nentities: ${gs.map.entityList.size}"}
                })

                it.row()

                val collapsibleTable = ExtTable()
                val collapsibleWidget = CollapsibleWidget(collapsibleTable, true)
                it.add(Table().also {
                    it.background = Utils.solidColorDrawable(Color.BLACK, 0.3f)

                    it.add(VisCheckBox("Show debug controls", false).also {
                        it.addChangeListener {
                            collapsibleWidget.isCollapsed = !collapsibleWidget.isCollapsed
                        }
                    })
                }).center().padTop(5f).fillX()

                it.row()
                it.add(collapsibleWidget)

                collapsibleTable.also {
                    it.defaults().left()
                    it.add(VisCheckBox("DEBUG", Engine.DEBUG).also {
                        it.addChangeListener {
                            Engine.DEBUG = !Engine.DEBUG
                        }
                    })

                    ExtLabel.defaultFont = VisUI.getSkin().getFont("small-font")
                    it.row().padTop(10f)

                    it.add(Utils.visUI_customCheckBox("DEBUG_MAP_CELLS", Engine.DEBUG_MAP_CELLS).also {
                        it.addChangeListener {
                            Engine.DEBUG_MAP_CELLS = !Engine.DEBUG_MAP_CELLS
                        }
                    })

                    it.row()

                    it.add(Utils.visUI_customCheckBox("DEBUG_ENTITIES", Engine.DEBUG_ENTITIES).also {
                        it.addChangeListener {
                            Engine.DEBUG_ENTITIES = !Engine.DEBUG_ENTITIES
                        }
                    })

                    it.row().padTop(10f)
                    it.add(Utils.visUI_customCheckBox("COLLISION_TREE_UPDATES", Engine.DEBUG_COLLISION_TREE_UPDATES).also {
                        it.addChangeListener {
                            Engine.DEBUG_COLLISION_TREE_UPDATES = !Engine.DEBUG_COLLISION_TREE_UPDATES
                        }
                    })
                    it.row()
                    it.add(Utils.visUI_customCheckBox("COLLISION_TREE_CELLS", Engine.DEBUG_COLLISION_TREE_CELLS).also {
                        it.addChangeListener {
                            Engine.DEBUG_COLLISION_TREE_CELLS = !Engine.DEBUG_COLLISION_TREE_CELLS
                        }
                    })

                    it.row().padTop(10f)
                    it.add(Utils.visUI_customCheckBox("STAGE_UI_ENTITY_POPUP_INFO", Engine.DEBUG_STAGE_UI_ENTITY_POPUP_INFO).also {
                        it.addChangeListener {
                            Engine.DEBUG_STAGE_UI_ENTITY_POPUP_INFO = !Engine.DEBUG_STAGE_UI_ENTITY_POPUP_INFO
                        }
                    })
                    it.row()
                    it.add(Utils.visUI_customCheckBox("STAGE_UI_ENTITY_INFO", Engine.DEBUG_STAGE_UI_ENTITY_INFO).also {
                        it.addChangeListener {
                            Engine.DEBUG_STAGE_UI_ENTITY_INFO = !Engine.DEBUG_STAGE_UI_ENTITY_INFO
                        }
                    })
                    it.row()
                    it.add(VisTextButton("Collapse All Entity Info").also {
                        it.addChangeListener {
                            for (ent in gs.map.entityList) {
                                ent.debugCollapsibleWidget.isCollapsed = true
                            }
                        }
                    })
                    ExtLabel.defaultFont = VisUI.getSkin().getFont("default-font")
                    it.row().padTop(10f)
                    it.add(VisTextButton("Expand All Entity Info").also {
                        it.addChangeListener {
                            for (ent in gs.map.entityList) {
                                ent.debugCollapsibleWidget.isCollapsed = false
                            }
                        }
                    })

                    it.row()

                    it.add(Utils.visUI_valueChangingSlider("camera zoom", 1f, 6f,
                            1f / gs.camera.zoom, 1f, 0) {
                        gs.camera.zoom = 1f / (Math.pow(2.0, it.toDouble() - 1.0)).toFloat()
                    })
                }
            })
        })
    }
}