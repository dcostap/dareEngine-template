package com.dcostap.pr.game

import com.badlogic.gdx.math.Vector2
import com.dcostap.engine.map.EntityTiledMap
import com.dcostap.engine.map.entities.Entity
import com.dcostap.engine.map.map_loading.EntityLoaderFromString

class GameEntityLoader(private val gameScreen: GameScreen) : EntityLoaderFromString {
    override fun loadEntityFromTiledTileObject(imageName: String, objectName: String, position: Vector2, widthPixels: Int, heightPixels: Int, map: EntityTiledMap): Entity {
        when (imageName) {

        }
        throw RuntimeException("Tried to load a TileObject with linked image: $objectName; there is no Entity associated with it")
    }

    override fun loadEntityFromObjectName(objectName: String, position: Vector2, widthPixels: Int, heightPixels: Int, map: EntityTiledMap): Entity {
        when (objectName) {

        }
        throw RuntimeException("Tried to load a Object with name: $objectName; there is no Entity associated with it")
    }
}
