package com.dcostap.engine

import com.badlogic.gdx.Application
import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.dcostap.engine.utils.DebugLog
import com.dcostap.engine.utils.Utils
import com.dcostap.engine.utils.actions.ActionsUpdater
import com.kotcrab.vis.ui.VisUI
import java.util.*

abstract class Engine : Game() {
    private var smoothedDelta = 1 / 60f

    lateinit var batch: SpriteBatch
        private set

    lateinit var gitTagVersion: String
        private set

    lateinit var gitCommit: String
        private set

//    var startupRun: () -> Unit = {}

    fun loadVersionProperties() {
        try {
            val versionProperties = Properties()
            versionProperties.load(Gdx.files.internal("version.properties").read())
            gitTagVersion = versionProperties.getProperty("version") ?: ""
            gitCommit = versionProperties.getProperty("commit") ?: ""
        } catch (exc: Exception) {
            printDebug(exc.message)
        }
    }

    var fixedDelta = 1 / 60f

    /** If true, it will ignore the app's delta */
    var useFixedDelta = true

    /** Override then create and load the Assets, set the Screen... */
    override fun create() {
        loadDistanceFactor()
        loadVersionProperties()

        batch = SpriteBatch()

        VisUI.load()

        debugScreen = DebugScreen()
        debugStage.clear()

        // generate pixel texture
        val pixmap = Pixmap(1, 1, Pixmap.Format.RGB888)
        pixmap.setColor(Color.WHITE)
        pixmap.drawPixel(0, 0)
        pixelTexture = TextureRegion(Texture(pixmap))

        pixmap.dispose()
    }

    /** Note that actions added here won't ever pause its progress. Note that Entities have a local
     * ActionsUpdater which might be a better fit*/
    val actions = ActionsUpdater()

    /** Measures render calls performed on each frame when [render] is called */
    var gameBatchRenderCalls = 0
        private set

    private var assetsSet = false

    override fun render() {
        if (!assetsSet) {
            assets = getAssets()
            assetsSet = true

            printDebug("version $gitTagVersion")
        }

        batch.totalRenderCalls = 0

        updateDelta()
        val delta = if (useFixedDelta) fixedDelta else smoothedDelta

        actions.update(delta)

        try {
            if (screen != null) screen.render(delta)
        } catch(e: Exception) {
            dispose()
            throw e
        }

        gameBatchRenderCalls = batch.totalRenderCalls
    }

    fun drawDebugScreen(delta: Float) {
        if (DEBUG && DEBUG_STAGE_UI) {
            debugScreen.render(delta)
        }
    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)

        debugScreen.resize(width, height)
    }

    abstract fun getAssets(): Assets

    private fun updateDelta() {
        // limit delta value
        var delta = Gdx.graphics.deltaTime
        delta = Utils.clamp(delta, delta, 1 / 25f)

        // smooth delta to avoid wonky movement at high speeds
        val smoothIncrement = 1 / 1000f
        if (smoothedDelta < delta) {
            smoothedDelta = Math.min(delta, smoothedDelta + smoothIncrement)
        } else if (smoothedDelta > delta) {
            smoothedDelta = Math.max(delta, smoothedDelta - smoothIncrement)
        }
    }

    override fun dispose() {
        getAssets().dispose()
        batch.dispose()
		if (screen != null) screen.dispose()

        VisUI.dispose()
        assets = null
//        currentGameDrawer = null
//        currentWorldViewport = null
        saveDebugLog()
        debugLog.debugLog.clear()
    }

    companion object {
//        /** If using a [BaseScreen], this will hold the current gameDrawer: it is set on [BaseScreen.show] and deleted on [BaseScreen.hide].
//         * Don't use this when rendering multiple BaseScreens together!
//         *
//         * Useful for drawing stuff in any place without having access to a Screen instance.
//         * @see [currentWorldViewport] */
//        var currentGameDrawer: GameDrawer? = null
//
//        /** @see [currentGameDrawer] */
//        var currentWorldViewport: Viewport? = null

        /** Pixels per game unit */
        var PPM_WIDTH: Int = 16
        var PPM_HEIGHT: Int = 16

        fun pixelsToUnitsWidth(pixels: Number): Float = pixels.toInt().toFloat() / PPM_WIDTH
        fun pixelsToUnitsHeight(pixels: Number): Float = pixels.toInt().toFloat() / PPM_HEIGHT

        var assets: Assets? = null

        var BASE_HEIGHT = 940
        var BASE_WIDTH = 680

        var DEBUG_STAGE_UI = true
        var DEBUG_STAGE_UI_ENTITY_POPUP_INFO = true
        var DEBUG_STAGE_UI_ENTITY_INFO = false

        var DEBUG = true

        var DEBUG_PRINT = true

        var DEBUG_MAP_CELLS = true
        var DEBUG_ENTITIES = true

        var DEBUG_COLLISION_TREE_CELLS = true
        var DEBUG_COLLISION_TREE_UPDATES = false
        var PATHFINDING_CELL_FLASH = true

        var DEBUG_LINE_THICKNESS = 0.05f
        var DEBUG_TRANSPARENCY = 0.5f

        /** By default it is a generated pixel, so no need to pack a pixel texture in the atlas, though it is encouraged
         * if you want a more performant debug rendering. Using the default pixel will ramp up the render calls. */
        lateinit var pixelTexture: TextureRegion

        private lateinit var debugScreen: DebugScreen
        val debugStage get() =  debugScreen.stage
        val debugDrawing get() =  debugScreen.debugDrawing

        /** This allows desktop density factor to be bigger, since eyes will be further away from screen than in a mobile device, it
         * is needed to increase the density factor by a bit; so it is multiplied by distance factor (default: 1.5) */
        private var distanceFactor = 1f

        /** Density Factor: difference of a standard density compared to current device's density.
         * Density means number of pixels per inch of screen. SmartPhones normally have higher density since
         * the screens are small but have big resolutions.
         *
         * Density factor is multiplied by distanceFactor. In computers distance factor
         * is bigger than in smartPhones, since you are further from screen; therefore density factor should be
         * higher (scale of things would be too small otherwise, you are not close to the screen like in a smartPhone!).
         * See [distanceFactor]
         *
         * Use this factor to scale things and keep them the same physical size. Bigger screen sizes will mean
         * more free space for things!  */
        val densityFactor: Float
            get() = Gdx.graphics.density * distanceFactor

        /** Resolution Factor: difference of current resolution's width compared to a base width.
         * Normally base width is [BASE_WIDTH].
         * If current resolution is double than the base, this would return 2.
         *
         * Use it to scale things up to all resolutions similar to a FitViewport.  */
        fun getResolutionFactor(baseResolutionWidth: Float): Float {
            return Gdx.graphics.width / baseResolutionWidth
        }

        /** Call once this class is loaded (otherwise calling Gdx.app.etc might crash the app) */
        private fun loadDistanceFactor() {
            when (Gdx.app.type) {
                Application.ApplicationType.Desktop -> distanceFactor = 1.5f
                else -> distanceFactor = 1f
            }
        }
    }
}

private val debugLog = DebugLog(500)
fun printDebug(string: String?) {
    debugLog.printIt = Engine.DEBUG_PRINT
    debugLog.printDebug(string)
}

fun printDebug(number: Number) {
    printDebug(number.toString())
}

fun saveDebugLog() {
    Gdx.files.local("debugLog.txt").writeString(debugLog.toString(), false)
}