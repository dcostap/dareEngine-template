package com.dcostap.engine.ui.utils

import com.badlogic.gdx.scenes.scene2d.ui.Button
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Skin

/**
 * Created by Darius on 10/04/2018.
 */
open class ExtButton(skin: Skin, label: Label? = null, styleName: String? = "default") : Button(skin, styleName) {
    var updateFunction: (delta: Float) -> Unit = {}

    /** Automatically enables the button if the function returns true; disables it otherwise.
     * Could be coded on the updateFunction, this is for convenience
     *
     * The updateFunction runs after this function so this might be overwritten
     *
     * Runs the provided function right away */
    var enableCondition: () -> Boolean = {!isDisabled}
        set(value) {
            field = value
            isDisabled = !value()
        }

    init {
        if (label != null) {
            add(label)
        }
    }

    override fun act(delta: Float) {
        super.act(delta)

        isDisabled = !enableCondition()

        updateFunction(delta)
    }
}
