package com.dcostap.engine.ui.utils

import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.dcostap.engine.utils.ifNotNull

/**
 * Created by Darius on 01/04/2018.
 *
 * Allows scaling to a size on one axis, automatically scales the other axis to keep the aspect ratio
 */
open class ExtImage(var region: TextureRegion) : Image(region) {
    var sizeWidth = 0
        set(value) {
            field = value
            width = value.toFloat()
        }

    var sizeHeight = 0
        set(value) {
            field = value
            height = value.toFloat()
        }

    init {
        region.ifNotNull {
            sizeWidth = it.regionWidth
            sizeHeight = it.regionHeight
        }
    }

    /** If image was scaled you will need to re-scale it again to adapt to new image */
    fun changeImage(texture: TextureRegion) {
        drawable = TextureRegionDrawable(texture)
        region = texture

        region.ifNotNull {
            sizeWidth = it.regionWidth
            sizeHeight = it.regionHeight
        }
    }

    var updateFunction: (delta: Float) -> Unit = {}

    fun scaleToWidth(width: Float) {
        sizeWidth = width.toInt()
        sizeHeight = (width * (region.regionHeight / region.regionWidth.toFloat())).toInt()
    }

    fun scaleToHeight(height: Float) {
        sizeHeight = height.toInt()
        sizeWidth = (height * (region.regionWidth / region.regionHeight.toFloat())).toInt()
    }

    /** Scale original texture's width and height by factor; Factor of 2 means double the size */
    fun scale(factor: Float) {
        sizeWidth = (region.regionWidth * factor).toInt()
        sizeHeight = (region.regionHeight * factor).toInt()
    }

    override fun getMinWidth(): Float {
        return sizeWidth.toFloat()
    }

    override fun getMinHeight(): Float {
        return sizeHeight.toFloat()
    }

    override fun getMaxWidth(): Float {
        return sizeWidth.toFloat()
    }

    override fun getMaxHeight(): Float {
        return sizeHeight.toFloat()
    }

    override fun act(delta: Float) {
        super.act(delta)

        updateFunction(delta)
    }
}