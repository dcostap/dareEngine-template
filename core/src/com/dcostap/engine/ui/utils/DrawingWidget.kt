package com.dcostap.engine.ui.utils

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Stage
import com.dcostap.engine.Assets
import com.dcostap.engine.utils.GameDrawer

/**
 * Created by Darius on 06/04/2018.
 */
open class DrawingWidget(assets: Assets, stage: Stage): OrderedTable() {
    val gameDrawer = GameDrawer(stage.batch)
    var drawingFunction: (self: DrawingWidget) -> Unit = {}

    override fun drawBackground(batch: Batch?, parentAlpha: Float, x: Float, y: Float) {
        super.drawBackground(batch, parentAlpha, x, y)
        drawingFunction(this)
    }
}
