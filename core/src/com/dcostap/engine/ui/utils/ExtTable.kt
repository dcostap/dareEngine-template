package com.dcostap.engine.ui.utils

import com.badlogic.gdx.scenes.scene2d.ui.Table

/**
 * Created by Darius on 19/04/2018.
 *
 * Provides an updateFunction
 */
open class ExtTable : Table() {
    var updateFunction: (delta: Float) -> Unit = {}

    override fun act(delta: Float) {
        super.act(delta)

        updateFunction(delta)
    }
}