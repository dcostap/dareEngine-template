package com.dcostap.engine.ui

import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.Window
import com.dcostap.engine.ui.utils.BlockInputTable
import com.dcostap.engine.utils.Drawable
import com.dcostap.engine.utils.GameDrawer
import com.dcostap.engine.utils.input.InputListener
import com.dcostap.engine.utils.modifyActorToBlockInputBeneathItself
import com.dcostap.engine.utils.moveToCenterOfStage

/**
 * Created by Darius on 19/11/2017
 *
 * Creates & manages a base UI layout. Holds the Scene2d Stage. Draws debug stuff of GameDrawer
 */
open class UIController(val skin: Skin, var stage: Stage, var debugFont: BitmapFont? = null) : Drawable, InputListener {
    lateinit var mainTable: Table
    lateinit var upTable: Table
    lateinit var centerLeftTable: Table
    lateinit var centerRightTable: Table
    lateinit var downTable: Table

    private lateinit var debugSelectionWindow: Window
    private var popUpWindow: Window? = null
    private var showSelectionWindowTimer = 0f

    lateinit private var blockInputTable: BlockInputTable

    init {
        createBaseUI()
    }

    /** updates & draws the Stage */
    override fun draw(gameDrawer: GameDrawer, delta: Float) {
//        updateUIDebug(delta)
        stage.act(delta)
        stage.draw()
    }

    private fun createBaseUI() {
        stage.isDebugAll = false

        mainTable = Table()
        mainTable.setFillParent(true)

        upTable = Table()
        centerLeftTable = Table()
        centerRightTable = Table()
        downTable = Table()

        mainTable.add(upTable).expandX().top().colspan(2)
        mainTable.row()
        mainTable.add(centerLeftTable).expand().left()
        mainTable.add(centerRightTable).expand().right()
        mainTable.row()
        mainTable.add(downTable).expandX().bottom().colspan(2)

        stage.addActor(mainTable)

        // table that will block input when needed
        blockInputTable = BlockInputTable()
        blockInputTable.isVisible = false
        stage.addActor(blockInputTable)
    }

    fun resetBaseUI() {
        upTable.clearChildren()
        centerLeftTable.clearChildren()
        centerRightTable.clearChildren()
        downTable.clearChildren()
    }

    /**
     * Automatically adds a window to the stage, adding a BlockInputTable to block input around the Window
     *
     * Automatically modifies the Window to catch input (so it's not propagated beneath it - but children can catch it)
     *
     * Automatically closes any other popUp-windows, and places the window in the center of the screen
     *
     * Use this instead of manually adding a Window to the stage, if you want those features
     */
    fun showPopUpWindow(popUpWindow: Window) {
        hidePopUpWindow()
        this.popUpWindow = popUpWindow
        stage.addActor(popUpWindow) // important that each Window is added after blockInputTable
        popUpWindow.pack()

        // center the window
        popUpWindow.moveToCenterOfStage()

        // to block inputs around the Window...
        blockInputTable.isVisible = true

        // to block inputs beneath the Window...
        popUpWindow.modifyActorToBlockInputBeneathItself()
    }

    /**
     * Hides the popUp-window, if there's any
     */
    fun hidePopUpWindow() {
        if (popUpWindow != null) {
            popUpWindow!!.remove()
            popUpWindow = null
        }

        blockInputTable.isVisible = false
    }

    /**
     * DebugSelectionWindow is used for debug purposes, so it's maintained by the main UIController
     */
    fun showDebugSelectionWindow(contents: Table) {
        debugSelectionWindow.clearChildren()

        debugSelectionWindow.add(contents)
        debugSelectionWindow.pack()

        debugSelectionWindow.isVisible = true
        debugSelectionWindow.setPosition(5f, 20f)
        debugSelectionWindow.isMovable = true

        showSelectionWindowTimer = 0.3f
    }

    override fun touchDownEvent(screenX: Float, screenY: Float, worldX: Float, worldY: Float, pointer: Int, isJustPressed: Boolean) {

    }

    override fun touchReleasedEvent(screenX: Float, screenY: Float, worldX: Float, worldY: Float, button: Int, pointer: Int) {
        if (showSelectionWindowTimer > 0) return

        debugSelectionWindow.isVisible = false
    }
}
