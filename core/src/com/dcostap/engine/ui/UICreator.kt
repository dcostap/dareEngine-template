package com.dcostap.engine.ui

import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.dcostap.engine.utils.Updatable

/**
 * Created by Darius on 20/11/2017
 *
 * UICreators create, update & draw UI, above UIController's base hud variables.
 * Needs to be updated. UI is drawn in UIController.
 */
abstract class UICreator(protected var uiController: UIController) : Updatable {
    protected var skin: Skin = uiController.skin

    abstract fun createUI()

    abstract override fun update(delta: Float)

    open fun deleteUI() {
        uiController.resetBaseUI()
    }
}
