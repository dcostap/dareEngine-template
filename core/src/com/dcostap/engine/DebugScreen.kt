package com.dcostap.engine

import com.badlogic.gdx.ScreenAdapter
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.ObjectMap
import com.badlogic.gdx.utils.Pools
import com.badlogic.gdx.utils.viewport.ScreenViewport
import com.badlogic.gdx.utils.viewport.Viewport
import com.dcostap.engine.utils.FlashingThing
import com.dcostap.engine.utils.GameDrawer
import com.dcostap.engine.utils.Utils
import com.dcostap.engine.utils.use

/**
 * Created by Darius on 28/12/2017.
 */
class DebugScreen() : ScreenAdapter() {
    val stage = Stage(ScreenViewport())
    private var didWarning = false
    val debugDrawing = DebugDrawing(stage)

    override fun render(delta: Float) {
        super.render(delta)

        stage.act(delta)
        stage.draw()

        val debugFont = Engine.assets?.getDebugFont()

        if (debugFont == null) {
            if (!didWarning) {
                com.dcostap.engine.printDebug("WARNING --> UIController wasn't assigned a debugFont so GameDrawer's debug draw stuff (text, rectangles, " +
                        "etc on UI) won't be drawn")
                didWarning = true
            }
            return
        }

        // draw GameDrawer debug stuff that is drawn on UI viewport
        debugDrawing.drawAllDebugStuff(debugFont, delta)
    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)
        stage.viewport.update(width, height, true)
    }

    override fun dispose() {
        stage.dispose()
    }
}

class DebugDrawing(val stage: Stage) {
    private val gameDrawer = GameDrawer(stage.batch)
    private var textStack = Array<ObjectMap.Entry<String, Vector2>>()
    private var flashingRectangles = ObjectMap<FlashingThing, Rectangle>()
    private var flashingLines = ObjectMap<FlashingThing, SavedLineToUI>()
    private var linesStack = Array<SavedLineToUI>()

    private var flashingText = ObjectMap<FlashingThing, ObjectMap.Entry<String, Vector2>>()

    private var dummyFlashingThings = Array<FlashingThing>()

    private fun defaultUIViewport() = Engine.debugStage.viewport

    /**
     * Save text in a list, with the world coords. The coords will be projected onto the UI screen
     *
     * As a result the text is drawn with the scale of the UI instead of the scale of the world viewport
     *
     * Call [drawStackOfTextInUI] later when drawing UI to draw all the saved text
     *
     * @param uiViewport If null no extra projection will be done. If the UI is in a ScreenViewport there will be
     * no problems, but in any other viewport the projection will be wrong.
     */
    fun drawTextInUI(worldX: Float, worldY: Float, text: String,
                     worldViewport: Viewport) {
        val entry = ObjectMap.Entry<String, Vector2>()
        entry.key = text

        val coords = Pools.obtain(Vector2::class.java)
        coords.set(Utils.projectPosition(worldX, worldY, worldViewport, defaultUIViewport()))
        entry.value = coords

        textStack.add(entry)
    }

    private inner class SavedLineToUI {
        lateinit var start: Vector2
        lateinit var end: Vector2
        var isArrow = false
    }

    /** @see drawTextInUI */
    fun drawLineInUI(x1: Float, y1: Float, x2: Float, y2: Float, isArrow: Boolean, flashingThing: FlashingThing? = null,
                     worldViewport: Viewport) {
        val initCorner = Pools.obtain(Vector2::class.java)
        val oppositeCorner = Pools.obtain(Vector2::class.java)

        initCorner.set(Utils.projectPosition(x1, y1, worldViewport, defaultUIViewport()))
        oppositeCorner.set(Utils.projectPosition(x2, y2, worldViewport, defaultUIViewport()))

        val savedLineToUI = SavedLineToUI()
        savedLineToUI.end = oppositeCorner
        savedLineToUI.start = initCorner
        savedLineToUI.isArrow = isArrow

        if (flashingThing == null) {
            linesStack.add(savedLineToUI)
        } else {
            flashingLines.put(flashingThing, savedLineToUI)
        }
    }

    /** @see drawTextInUI */
    fun drawFlashingRectangleInUI(rectangle: Rectangle, flashingThing: FlashingThing,
                                  worldViewport: Viewport) {
        val initCorner = Pools.obtain(Vector2::class.java)
        val oppositeCorner = Pools.obtain(Vector2::class.java)
        initCorner.set(rectangle.x, rectangle.y)
        oppositeCorner.set(rectangle.x + rectangle.width, rectangle.y + rectangle.height)

        initCorner.set(Utils.projectPosition(initCorner.x, initCorner.y, worldViewport, defaultUIViewport()))
        oppositeCorner.set(Utils.projectPosition(oppositeCorner.x, oppositeCorner.y, worldViewport, defaultUIViewport()))

        oppositeCorner.x -= initCorner.x
        oppositeCorner.y -= initCorner.y
        rectangle.set(initCorner.x, initCorner.y, oppositeCorner.x, oppositeCorner.y)

        Pools.free(initCorner)
        Pools.free(oppositeCorner)

        flashingRectangles.put(flashingThing, rectangle)
    }

    /** @see drawTextInUI */
    fun drawFlashingTextInUI(text: String, x: Float, y: Float, flashingThing: FlashingThing,
                             worldViewport: Viewport) {
        val position = Pools.obtain(Vector2::class.java)
        position.set(Utils.projectPosition(x, y, worldViewport, defaultUIViewport()))

        val entry = ObjectMap.Entry<String, Vector2>()
        entry.value = position
        entry.key = text
        flashingText.put(flashingThing, entry)
    }

    /** Call after the batch used by this GameDrawer is setup to have UI's projection */
    fun drawAllDebugStuff(font: BitmapFont, delta: Float) {
        gameDrawer.batch.setProjectionMatrix(stage.camera.combined)
        gameDrawer.batch.use {
            drawStackOfTextInUI(font)
            drawStackOfFlashingRectanglesInUI(delta)
            drawStackOfFlashingTextInUI(font, delta)
            drawStackOfFlashingLinesInUI(delta)
            drawStackOfLinesInUI()
        }
    }

    private fun drawStackOfTextInUI(font: BitmapFont) {
        for (entry in textStack) {
            gameDrawer.drawText(font, entry.value.x, entry.value.y, entry.key)
            Pools.free(entry.value)
        }

        textStack.clear()
    }

    private fun drawStackOfLinesInUI() {
        for (entry in linesStack) {
            if (entry.isArrow) {
                gameDrawer.drawLine(entry.start, entry.end, 2f, isArrow = true, arrowSize = 18f)
            } else {
                gameDrawer.drawLine(entry.start, entry.end, 2f)
            }

            Pools.free(entry.start)
            Pools.free(entry.end)
        }

        linesStack.clear()
    }

    private fun drawStackOfFlashingRectanglesInUI(delta: Float) {
        dummyFlashingThings.clear()
        for (entry in flashingRectangles) {
            entry.key.update(delta)
            if (!entry.key.isFlashing) {
                dummyFlashingThings.add(entry.key)
            } else {
                entry.key.setupGameDrawer(gameDrawer)
                gameDrawer.drawRectangle(entry.value, 0f, true)
                entry.key.resetGameDrawer(gameDrawer)
            }
        }

        for (flashingThing in dummyFlashingThings) {
            flashingRectangles.remove(flashingThing)
        }
    }

    private fun drawStackOfFlashingLinesInUI(delta: Float) {
        dummyFlashingThings.clear()
        for (entry in flashingLines) {
            entry.key.update(delta)
            if (!entry.key.isFlashing) {
                dummyFlashingThings.add(entry.key)
            } else {
                entry.key.setupGameDrawer(gameDrawer)
                if (entry.value.isArrow) {
                    gameDrawer.drawLine(entry.value.start, entry.value.end, 2f, isArrow = true, arrowSize = 18f)
                } else {
                    gameDrawer.drawLine(entry.value.start, entry.value.end, 2f)
                }
                entry.key.resetGameDrawer(gameDrawer)
            }
        }

        for (flashingThing in dummyFlashingThings) {
            Pools.free(flashingLines.get(flashingThing).start)
            Pools.free(flashingLines.get(flashingThing).end)
            flashingLines.remove(flashingThing)
        }
    }

    private fun drawStackOfFlashingTextInUI(font: BitmapFont, delta: Float) {
        dummyFlashingThings.clear()
        for (flashingThing in flashingText.keys()) {
            flashingThing.update(delta)
            if (!flashingThing.isFlashing) {
                dummyFlashingThings.add(flashingThing)
            } else {
                flashingThing.setupGameDrawer(gameDrawer)
                gameDrawer.drawText(font, flashingText.get(flashingThing).value.x, flashingText.get(flashingThing).value.y,
                        flashingText.get(flashingThing).key)
                flashingThing.resetGameDrawer(gameDrawer)
            }
        }

        for (flashingThing in dummyFlashingThings) {
            Pools.free(flashingText.get(flashingThing).value)
            flashingText.remove(flashingThing)
        }
    }
}
