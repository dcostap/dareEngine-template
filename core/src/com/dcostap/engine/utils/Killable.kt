package com.dcostap.engine.utils

/**
 * Created by Darius on 23/11/2017
 */
interface Killable {
    fun kill()
    val isKilled: Boolean
}
