package com.dcostap.engine.utils

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.dcostap.engine.Engine

/**
 * Created by Darius on 14/09/2017.
 *
 * Helper class to draw stuff in a specific viewport. By default it adapts textures drawn to the global [Engine.PPM_WIDTH].
 *
 * Use it with a **scaling viewport** with arbitrary units (so 1 unit = PPM pixels):
 *
 * * Automatically scales all textures to the PPM, so when drawing 1 pixel isn't 1 unit
 *
 * Note: you shouldn't draw fonts in scaling viewports, do it in another viewport
 *
 * Use it with a **ScreenViewport** that adjusts to different resolutions using DensityFactor
 * (so higher resolution != smaller images, and so physical size of drawn things stays the same):
 *
 * * Use drawScaled to draw images that scale to comply with DensityFactor, based on an initial size
 *
 * * Draw normally with a font, if that font is generated in Assets with size based on DensityFactor
 *
 * If you use **Scene2d**, adapt to density factor this way:
 *
 * * use stage's actor (ExtImage) to draw images, will work the same way as using drawScaled
 *
 * * with 9patches (button / pane / widget graphics), create them with a base size multiplied by Engine.getDensityFactor
 *
 * * use fonts the same way as before :D
 *
 * * If you don't want it to scale textures based on the PPM** (scaling viewport with no arbitrary units),
 * change the global [Engine.PPM_WIDTH] to 1
 */
class GameDrawer(val batch: Batch) {
    var alpha = 1f
        set(value) {
            field = value
            updateDrawingColorAndAlpha()
        }

    /** won't modify alpha */
    var color = Color.WHITE
        set(value) {
            field = value
            updateDrawingColorAndAlpha()
        }

    /** won't modify alpha */
    fun setColor(r: Float, g: Float, b: Float) {
        color.r = r
        color.g = g
        color.b = b

        updateDrawingColorAndAlpha()
    }

    /** modifies alpha too */
    fun setColorAndAlpha(r: Float, g: Float, b: Float, a: Float) {
        color.r = r
        color.g = g
        color.b = b
        alpha = a

        updateDrawingColorAndAlpha()
    }

    fun resetColor() {
        this.color = Color.WHITE
        updateDrawingColorAndAlpha()
    }

    fun resetAlpha() {
        this.alpha = 1f
        updateDrawingColorAndAlpha()
    }

    fun resetColorAndAlpha() {
        this.resetAlpha()
        this.resetColor()
    }

    private fun updateDrawingColorAndAlpha() = batch.setColor(color.r, color.g, color.b, alpha)

    private var dummyVector2 = Vector2()

    var drawingDisplacement = Vector2(0f, 0f)
        private set
    fun setDrawingDisplacementX(x: Float) {
        drawingDisplacement.x = x
    }
    fun setDrawingDisplacementY(y: Float) {
        drawingDisplacement.y = y
    }
    fun setDrawingDisplacementXY(xy: Float) {
        drawingDisplacement.set(xy, xy)
    }
    fun setDrawingDisplacementPixels(x: Float, y: Float) {
        drawingDisplacement.set(Engine.pixelsToUnitsWidth(x), Engine.pixelsToUnitsWidth(y))
    }
    fun setDrawingDisplacementPixelsXY(xy: Float) {
        drawingDisplacement.set(Engine.pixelsToUnitsWidth(xy), Engine.pixelsToUnitsWidth(xy))
    }

    fun resetDrawingDisplacement() {
        drawingDisplacement.set(0f, 0f)
    }
    val defaultScaleX = 1f
    val defaultScaleY = 1f

    val defaultRotation = 0f

    /** Local scaleX, used when no scaleX is specified on the draw methods */
    var scaleX = defaultScaleX

    /** Local scaleY, used when no scaleY is specified on the draw methods */
    var scaleY = defaultScaleY

    fun setScaleXY(value: Float) {
        scaleX = value
        scaleY = value
    }

    /** Local rotation, used when no rotation is specified on the draw methods */
    var rotation = defaultRotation

    /** Resets local scale and rotation to the default values */
    fun resetModifiers() {
        scaleX = defaultScaleX
        scaleY = defaultScaleY
        rotation = defaultRotation
    }

    fun getCorrectX(x: Float): Float {
        return x + drawingDisplacement.x
    }

    fun getCorrectY(y: Float): Float {
        return y + drawingDisplacement.y
    }

    var useCustomPPM = false
    var customPPMwidth = 16
    var customPPMheight = 16

    /** the actual PPM value used by this GameDrawer. Will be [Engine.PPM_WIDTH] if [useCustomPPM] is false */
    val usedPPMwidth = if (useCustomPPM) customPPMwidth else Engine.PPM_WIDTH
    val usedPPMheight = if (useCustomPPM) customPPMheight else Engine.PPM_HEIGHT

    /** returns the amount the texture needs to be scaled to be drawn according to the Pixels Per Meter (PPM) constant **/
    fun getUnitWidth(textureRegion: TextureRegion) = textureRegion.regionWidth / usedPPMwidth.toFloat()

    fun getUnitHeight(textureRegion: TextureRegion) = textureRegion.regionHeight / usedPPMheight.toFloat()

    fun draw(textureRegion: TextureRegion, x: Float, y: Float, rotation: Float = this.rotation, scaleX: Float = this.scaleX,
             scaleY: Float = this.scaleY, originX: Float = 0f, originY: Float = 0f)
    {
        val thisX = getCorrectX(x)
        val thisY = getCorrectY(y)

        val width = getUnitWidth(textureRegion)
        val height = getUnitHeight(textureRegion)

        batch.draw(textureRegion, thisX, thisY, originX, originY,
                width, height, scaleX, scaleY, rotation)
    }

    fun draw(textureRegion: TextureRegion, vector2: Vector2, rotation: Float = this.rotation, scaleX: Float = this.scaleX,
             scaleY: Float = this.scaleY, originX: Float = 0f, originY: Float = 0f)
    {
        this.draw(textureRegion, vector2.x, vector2.y, rotation, scaleX, scaleY, originX, originY)
    }

    /** With custom size */
    fun drawSized(textureRegion: TextureRegion, x: Float, y: Float, width: Float, height: Float,
             rotation: Float = this.rotation, scaleX: Float = this.scaleX,
             scaleY: Float = this.scaleY, originX: Float = 0f, originY: Float = 0f)
    {
        val thisX = getCorrectX(x)
        val thisY = getCorrectY(y)

        batch.draw(textureRegion, thisX, thisY, originX, originY,
                width, height, scaleX, scaleY, rotation)
    }

    /**
     * Automatically sets the origin in the center of the texture, either on X, Y or both. This allows for scaling &
     * rotation to happen around that origin.
     *
     * Position of drawing will **not** be at origin, but still at X and Y
     * coordinates specified. To draw the image centered, that is, having the center of the texture being the X and Y
     * coordinates, use [drawCentered]
     */
    fun drawWithOriginOnCenter(textureRegion: TextureRegion, x: Float, y: Float, scaleX: Float = this.scaleX,
                               scaleY: Float = this.scaleY, centerOriginOnXAxis: Boolean = false, centerOriginOnYAxis: Boolean = false,
                               rotationDegrees: Float = this.rotation)
    {
        this.drawCenteredWithOriginOnCenter(textureRegion, x, y, scaleX, scaleY, centerOriginOnXAxis = centerOriginOnXAxis,
                centerOriginOnYAxis = centerOriginOnYAxis, rotationDegrees = rotationDegrees)
    }

    /**
     * Offsets the drawing by half of its size so the origin of the input X and Y is in the middle of the texture, thus
     * the texture is being drawn centered on input X and Y
     */
    fun drawCentered(textureRegion: TextureRegion, x: Float, y: Float, scaleX: Float = this.scaleX, scaleY: Float = this.scaleY,
                             centerOnXAxis: Boolean = true, centerOnYAxis: Boolean = true, rotationDegrees: Float = this.rotation)
    {
        this.drawCenteredWithOriginOnCenter(textureRegion, x, y, scaleX, scaleY,
                centerOnXAxis, centerOnYAxis, false, false, rotationDegrees)
    }

    /**
     * Combines centering of origin with centering of image. Origin centered allows for rotation and scaling to happen around
     * it. Centering of image allows the image to be drawn with input X and Y being in its middle, thus centered on those
     * coordinates.
     */
    fun drawCenteredWithOriginOnCenter(textureRegion: TextureRegion, x: Float, y: Float, scaleX: Float = this.scaleX,
                                       scaleY: Float = this.scaleY, centerOnXAxis: Boolean = false, centerOnYAxis: Boolean = false,
                                       centerOriginOnXAxis: Boolean = false, centerOriginOnYAxis: Boolean = false,
                                       rotationDegrees: Float = this.rotation)
    {
        val width = getUnitWidth(textureRegion)
        val height = getUnitHeight(textureRegion)

        fun hasToCenter() = centerOnXAxis || centerOnYAxis

        val previousDisplacement = dummyVector2
        previousDisplacement.set(drawingDisplacement)

        if (hasToCenter()) resetDrawingDisplacement()
        if (centerOnXAxis) setDrawingDisplacementX(-width / 2f)
        if (centerOnYAxis) setDrawingDisplacementY(-height / 2f)

        val thisX = getCorrectX(x)
        val thisY = getCorrectY(y)

        batch.draw(textureRegion, thisX, thisY, if (!centerOriginOnXAxis) 0f else (width / 2f),
                if (!centerOriginOnYAxis) 0f else (height / 2f),
                width, height, scaleX, scaleY, rotationDegrees)

        drawingDisplacement.set(previousDisplacement)
    }

    /**
     * Draws an image scaled to the nearest non-decimal scale factor, based on the densityFactor / resolutionFactor of the app
     * This means that the images will -almost- always have the same physical size on all devices
     *
     * Use it with ScreenViewport - since with a scaling viewport the above can't be achieved
     * @param useDensityFactor if false will use resolution factor
     */
    fun drawScaled(textureRegion: TextureRegion, x: Float, y: Float, baseSize: Float, useDensityFactor: Boolean) {
        val thisX = getCorrectX(x)
        val thisY = getCorrectY(y)

        val scaleFactor = getImageScaleFactor(baseSize, textureRegion.regionWidth.toFloat(),
                textureRegion.regionHeight.toFloat(), useDensityFactor)

        batch.draw(textureRegion, thisX, thisY, (textureRegion.regionWidth * scaleFactor).toFloat(),
                (textureRegion.regionHeight * scaleFactor).toFloat())
    }

    fun drawText(bitmapFont: BitmapFont, x: Float, y: Float, text: String) {
        bitmapFont.color = this.color
        bitmapFont.draw(batch, text, x, y)
        bitmapFont.color = Color.WHITE
    }

    /**
     * Draws a Rectangle using "pixel" image on atlas
     *
     * @param x         bottom-left corner x
     * @param y         bottom-left corner y
     * @param thickness size of the borders if the rectangle is not filled
     * @param fill      whether the rectangle drawn will be filled with the color
     */
    fun drawRectangle(x: Float, y: Float, width: Float, height: Float, thickness: Float, fill: Boolean,
                      originX: Float = 0f, originY: Float = 0f, scaleX: Float = this.scaleX, scaleY: Float = this.scaleY,
                      rotation: Float = this.rotation) {
        val thisX = getCorrectX(x)
        val thisY = getCorrectY(y)

        if (!fill) {
            batch.draw(Engine.pixelTexture, thisX, thisY, originX, originY, width, thickness, scaleX, scaleY, rotation)
            batch.draw(Engine.pixelTexture, thisX, thisY, originX, originY, thickness, height, scaleX, scaleY, rotation)
            batch.draw(Engine.pixelTexture, thisX, thisY + height - thickness, originX, originY, width, thickness, scaleX, scaleY, rotation)
            batch.draw(Engine.pixelTexture, thisX + width - thickness, thisY, originX, originY, thickness, height, scaleX, scaleY, rotation)
        } else {
            batch.draw(Engine.pixelTexture, thisX, thisY, originX, originY, width, height, scaleX, scaleY, rotation)
        }
    }

    fun drawRectangle(rectangle: Rectangle, thickness: Float, fill: Boolean) {
        this.drawRectangle(rectangle.x, rectangle.y, rectangle.width, rectangle.height, thickness, fill)
    }

    fun drawCross(x: Float, y: Float, axisSize: Float, thickness: Float) {
        this.drawLine(x - axisSize, y, x + axisSize, y, thickness)
        this.drawLine(x, y - axisSize, x, y + axisSize, thickness)
    }

    /**
     * Draws a line using "pixel" image on atlas. Lines drawn together will not be correctly joined.
     *
     * @param x1        start x
     * @param y1        start y
     * @param x2        end x
     * @param y2        end y
     * @param thickness size of the line
     */
    fun drawLine(x1: Float, y1: Float, x2: Float, y2: Float, thickness: Float, isArrow: Boolean = false, arrowSize: Float = 0.5f) {
        val thisX1 = getCorrectX(x1)
        val thisX2 = getCorrectX(x2)
        val thisY1 = getCorrectY(y1)
        val thisY2 = getCorrectY(y2)

        val dx = thisX2 - thisX1
        val dy = thisY2 - thisY1
        val dist = Math.sqrt((dx * dx + dy * dy).toDouble()).toFloat()
        val deg = Math.toDegrees(Math.atan2(dy.toDouble(), dx.toDouble())).toFloat()
        batch.draw(Engine.pixelTexture, thisX1, thisY1, 0f, thickness / 2f, dist, thickness, 1f, 1f, deg)

        if (isArrow) {
            val angle = Math . toRadians (Utils.getAngleBetweenPoints(x1, y1, x2, y2).toDouble())
            val angleDiff = -40
            val angle1 = angle +angleDiff
            val angle2 = angle -angleDiff
            drawLine(x2, y2, (x2 + (Math.cos(angle1) * arrowSize)).toFloat(), (y2 + (Math.sin(angle1) * arrowSize)).toFloat(), thickness)
            drawLine(x2, y2, (x2 + (Math.cos(angle2) * arrowSize)).toFloat(), (y2 + (Math.sin(angle2) * arrowSize)).toFloat(), thickness)
        }
    }

    fun drawLine(start: Vector2, end: Vector2, thickness: Float, isArrow: Boolean = false, arrowSize: Float = 0.5f) {
        this.drawLine(start.x, start.y, end.x, end.y, thickness, isArrow, arrowSize)
    }

    fun drawLineFromAngle(x1: Float, y1: Float, distance: Float, angleDegrees: Float, thickness: Float) {
        this.drawLine(x1, y1, ((x1 + distance * Math.cos(Math.toRadians(angleDegrees.toDouble()))).toFloat()),
                ((y1 + distance * Math.sin(Math.toRadians(angleDegrees.toDouble()))).toFloat()), thickness)
    }

    companion object {
        /**
         * Gets the nearest non-decimal scale factor for the image, based on the densityFactor / resolutionFactor of the app
         * @param useDensityFactor if false will use resolution factor
         */
        fun getImageScaleFactor(size: Float, imageWidthPixels: Float, imageHeightPixels: Float,
                                useDensityFactor: Boolean = true, resolutionFactorBaseWidth: Int = Engine.BASE_WIDTH): Int
        {
            val pixels: Float =
                    if (useDensityFactor)
                        Engine.densityFactor * size
                    else
                        Engine.getResolutionFactor(resolutionFactorBaseWidth.toFloat())

            return Math.round(pixels / Math.max(imageHeightPixels, imageWidthPixels))
        }
    }
}