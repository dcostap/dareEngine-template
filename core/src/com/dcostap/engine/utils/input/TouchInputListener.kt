package com.dcostap.engine.utils.input

/**
 * Created by Darius on 19/11/2017.
 */
interface InputListener {
    /**
     * **WARNING: you can't distinguish mouse buttons in this method, because it is unsupported**
     *
     * *(mouse buttons share the same pointer, so [Touch.button] behaves erratically when more than one of those buttons are pressed)*
     *
     * *If you wanna know which mouse button is pressed / just pressed, use [InputController.isTouchPressed] instead*
     *
     * Called when the touch is first pressed until it is released
     *
     * @param screenX with origin on bottom-left
     * @param screenY with origin on bottom-left
     * @param pointer ID of the finger pressed; always 0 if it's a mouse in desktop
     * Various events can be issued at the same time, one for each pointer;
     * ignore pointers other than 0 if you don't want to potentially have repeated calls of the same method
     */
    fun touchDownEvent(screenX: Float, screenY: Float, worldX: Float, worldY: Float, pointer: Int, isJustPressed: Boolean)

    /**
     * @see touchDownEvent
     */
    fun touchReleasedEvent(screenX: Float, screenY: Float, worldX: Float, worldY: Float, button: Int, pointer: Int)
}
