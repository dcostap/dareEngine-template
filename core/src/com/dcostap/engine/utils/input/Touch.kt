package com.dcostap.engine.utils.input

import com.badlogic.gdx.math.Vector2

/**
 * Created by Darius on 20/01/2018
 */
data class Touch(var screenX: Float, var screenY: Float, var worldX: Float, var worldY: Float,
                 var button: Int, var isJustPressed: Boolean, var wasJustPressed: Boolean = false, var isJustReleased: Boolean = false, var wasJustReleased: Boolean = false)

class Mouse {
    val world = Vector2()
    val screen = Vector2()

    val screenX get() = screen.x
    val screenY get() = screen.y
    val worldX get() = world.x
    val worldY get() = world.y
}
