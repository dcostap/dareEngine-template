package com.dcostap.engine.utils.actions

/**
 * Created by Darius on 15/04/2018.
 */
class RunAction(delay: Float = 0f, val function: () -> Unit)
    : Action(delay) {
    override fun updateProgress(progress: Float) {

    }

    override fun finalUpdate() {
        function()
    }
}