package com.dcostap.engine.utils.actions

/**
 * Created by Darius on 15/04/2018.
 */
class SequenceAction(vararg actions: Action) : Action(0f) {
    val sequenceOfActions = actions

    override fun updateProgress(progress: Float) {

    }

    override fun finalUpdate() {

    }

    override fun reset() {
        index = 0
    }

    var index = 0

    override fun update(delta: Float) {
        if (hasFinished) return
        sequenceOfActions[index].update(delta)
        if (sequenceOfActions[index].hasFinished) index++
    }

    override val hasFinished: Boolean
        get() = index == sequenceOfActions.size
}
