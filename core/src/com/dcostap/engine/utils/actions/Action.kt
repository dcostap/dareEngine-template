package com.dcostap.engine.utils.actions

import com.badlogic.gdx.math.Interpolation
import com.dcostap.engine.utils.Interpolator
import com.dcostap.engine.utils.Updatable

/**
 * Created by Darius on 15/04/2018.
 */
abstract class Action(duration: Float, interpolation: Interpolation = Interpolation.linear): Updatable {
    val interpolator = Interpolator(interpolation, duration, 0f, 1f)
    var pauseTimer = false
    private var finalUpdateDone = false

    override fun update(delta: Float) {
        if (interpolator.hasFinished) {
            if (finalUpdateDone) return

            finalUpdate()
            finalUpdateDone = true
            return
        }

        if (!pauseTimer) interpolator.update(delta)
        updateProgress(interpolator.getValue())
    }

    /** @param progress Progress from 0 to 1 (completed) affected by interpolation. Note that interpolation is already applied */
    abstract fun updateProgress(progress: Float)

    /** Always runs before finishing, even if the duration is 0 */
    abstract fun finalUpdate()

    fun forceFinish() {
        interpolator.timer.elapsed = interpolator.timer.timeLimit
    }

    /** Prepares the Action to be reused, reset all variables */
    open fun reset() {
        interpolator.timer.elapsed = 0f
        finalUpdateDone = false
    }

    open val hasFinished: Boolean
        get() = interpolator.hasFinished && finalUpdateDone

    companion object {
        fun runAction(delay: Float, function: () -> Unit) = RunAction(delay, function)
        fun runAction(function: () -> Unit) = RunAction(0f, function)
        fun sequenceAction(vararg actions: Action) = SequenceAction(*actions)
        fun valueAction(initialValue: Float, endValue: Float, duration: Float, interpolation: Interpolation)
                = ValueAction(initialValue, endValue, duration, interpolation)
    }
}
