package com.dcostap.engine.utils.actions

import com.badlogic.gdx.math.Interpolation
import com.dcostap.engine.utils.Utils

/**
 * Created by Darius on 04/05/2018.
 *
 * Interpolates a number from one initial value to an end value during the duration of the Action
 */
class ValueAction(val initialValue: Float, val endValue: Float, duration: Float, interpolation: Interpolation)
    : Action(duration, interpolation) {
    var value = initialValue
        private set

    override fun updateProgress(progress: Float) {
        value = Utils.mapToRange(progress, 0f, 1f, initialValue, endValue)
    }

    override fun reset() {
        super.reset()

        value = initialValue
    }

    override fun finalUpdate() {
        value = initialValue
    }
}
