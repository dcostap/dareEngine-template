package com.dcostap.engine.utils

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Preferences
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.glutils.FrameBuffer
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Action
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Cell
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.utils.BaseDrawable
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.Drawable
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.JsonValue
import com.badlogic.gdx.utils.viewport.Viewport
import com.dcostap.engine.Engine
import com.dcostap.engine.ui.utils.ExtLabel
import com.dcostap.engine.ui.utils.ExtTable
import com.kotcrab.vis.ui.widget.VisCheckBox
import com.kotcrab.vis.ui.widget.VisSlider
import java.io.PrintWriter
import java.io.StringWriter
import java.util.*

/** Substitute of kotlin's .let; call after a variable to execute block of code (only when the variable isn't null)
 * with local non-nullable copy of the variable: this avoids the "variable could have been modified" complaint from the compiler
 *
 * Just like with .let, inside of the block reference reference the variable with **it**
 *
 * Equivalent using .let: **variable?.let {}**; with this: **variable.ifNotNull {}** */
fun <T : Any?> T?.ifNotNull(f: (it: T) -> Unit): Unit? {
    return if (this != null) f(this) else null
}

fun <T1: Any, T2: Any> ifNotNull(p1: T1?, p2: T2?, block: (T1, T2)->Unit): Unit? {
    return if (p1 != null && p2 != null) block(p1, p2) else null
}

fun <T1: Any, T2: Any, T3: Any> ifNotNull(p1: T1?, p2: T2?, p3: T3?, block: (T1, T2, T3)->Unit): Unit? {
    return if (p1 != null && p2 != null && p3 != null) block(p1, p2, p3) else null
}

fun <T : Any> T?.ifNull(f: () -> Unit): Unit? {
    return if (this == null) f() else null
}

/** If any is null */
fun <T1: Any, T2: Any> ifNull(p1: T1?, p2: T2?, block: ()->Unit): Unit? {
    return if (p1 == null || p2 == null) block() else null
}

/** If any is null */
fun <T1: Any, T2: Any, T3: Any> ifNull(p1: T1?, p2: T2?, p3: T3?, block: ()->Unit): Unit? {
    return if (p1 == null || p2 == null || p3 == null) block() else null
}

fun <T : Actor> Cell<T>.padTopBottom(pad: Float): Cell<T> {
    this.padTop(pad)
    return this.padBottom(pad)
}

fun Actor.modifyActorToBlockInputBeneathItself() {
    this.touchable = Touchable.enabled

    this.addListener(object : ClickListener() {
        override fun clicked(event: InputEvent?, x: Float, y: Float) {

        }
    })
}

/** Puts the scene2d actor in the center of the stage. Call .pack() on the actor before; needs to know its final size  */
fun Actor.moveToCenterOfStage() {
    this.setPosition(((stage.width - this.width) / 2f).toInt().toFloat(),
            ((stage.height - this.height) / 2f).toInt().toFloat())
}

fun Exception.getFullString(): String {
    val sw = StringWriter()
    this.printStackTrace(PrintWriter(sw))
    val exceptionAsString = sw.toString()
    return exceptionAsString
}

fun JsonValue.addChildValue(name: String = "", value: Any) {
    when (value) {
        is Boolean -> addChild(name, JsonValue(value))
        is String -> addChild(name, JsonValue(value))
        is Int -> addChild(name, JsonValue(value.toLong()))
        is Long -> addChild(name, JsonValue(value))
        is Float -> addChild(name, JsonValue(value.toDouble()))
        is Double -> addChild(name, JsonValue(value))
        is JsonValue -> addChild(name, value)
        else -> throw RuntimeException("Tried to add value: $value (type: ${value.javaClass}) to json; but value has type not supported ")
    }
}

fun JsonValue.getChildValue(name: String = "", defaultValue: Any): Any {
    return when (defaultValue) {
        is Boolean -> getBoolean(name, defaultValue)
        is String -> getString(name, defaultValue)
        is Int -> getInt(name, defaultValue)
        is Long -> getLong(name, defaultValue)
        is Float -> getFloat(name, defaultValue)
        is Double -> getDouble(name, defaultValue)
        is JsonValue -> getChild(name)
        else -> throw RuntimeException("Tried to get value with name $name. Not found")
    }
}

fun <T : Actor> Cell<T>.padLeftRight(pad: Float): Cell<T> {
    this.padLeft(pad)
    return this.padRight(pad)
}

fun Table.padTopBottom(pad: Float): Table {
    this.padTop(pad)
    return this.padBottom(pad)
}

fun Table.padLeftRight(pad: Float): Table {
    this.padLeft(pad)
    return this.padRight(pad)
}

fun percentOfAppWidthInUnits(percent: Number) = Engine.pixelsToUnitsWidth(Utils.percentageInt(Gdx.graphics.width.toFloat(), percent.toFloat()).toFloat())
fun percentOfAppHeightInUnits(percent: Number) = Engine.pixelsToUnitsWidth(Utils.percentageInt(Gdx.graphics.height.toFloat(), percent.toFloat()).toFloat())

/** @return In viewport units */
fun percentOfViewportWidth(percent: Number, viewport: Viewport): Float {
    return (Utils.percentageInt(viewport.worldWidth, percent.toFloat()).toFloat()) * (viewport.camera as OrthographicCamera).zoom
}
/** @return In viewport units */
fun percentOfViewportHeight(percent: Number, viewport: Viewport): Float {
    return (Utils.percentageInt(viewport.worldHeight, percent.toFloat()).toFloat()) * (viewport.camera as OrthographicCamera).zoom
}

/** Rectangle constructor with default parameters */
fun newRectangle(x: Number = 0f, y: Number = 0f, width: Number = 0f, height: Number = 0f): Rectangle {
    return Rectangle().also {it.x = x.toFloat(); it.y = y.toFloat(); it.width = width.toFloat(); it.height = height.toFloat()}
}
/** Translates input into Engine units (Engine.PPM) */
fun newRectanglePixels(xPixels: Number = 0f, yPixels: Number = 0f, widthPixels: Number = 0f, heightPixels: Number = 0f): Rectangle {
    return Rectangle().also {
        it.x = Engine.pixelsToUnitsWidth(xPixels.toFloat()); it.y = Engine.pixelsToUnitsWidth(yPixels.toFloat())
        it.width = Engine.pixelsToUnitsWidth(widthPixels.toFloat()); it.height = Engine.pixelsToUnitsWidth(heightPixels.toFloat())
    }
}

/** Extension to add support for doubles in Preferences. From https://stackoverflow.com/a/45412036 */
fun Preferences.putDouble(key: String, double: Double) =
        putLong(key, java.lang.Double.doubleToRawLongBits(double))

fun Preferences.getDouble(key: String, default: Double) =
        java.lang.Double.longBitsToDouble(getLong(key, java.lang.Double.doubleToRawLongBits(default)))

/** Extension for Button */
infix fun Actor.addChangeListener(function: () -> Unit) {
    this.addListener(object: ChangeListener() {
        override fun changed(event: ChangeEvent?, actor: Actor?) {
            function()
        }
    })
}

infix fun <T : Actor> Table.add(actor: T): Cell<T> {
    return this.add(actor)
}

fun Rectangle.pixelsToGameUnits(): Rectangle {
    // translate bounding box pixels to units
    x /= Engine.PPM_WIDTH
    y /= Engine.PPM_HEIGHT

    // extra translation to units -> width and height
    width /= Engine.PPM_WIDTH
    height /= Engine.PPM_HEIGHT

    return this
}

/** Percentage of application's height, rounded to closest integer */
fun percentOfAppHeight(percent: Float) = Utils.percentageInt(Gdx.graphics.height.toFloat(), percent).toFloat()
/** Percentage of application's width, rounded to closest integer */
fun percentOfAppWidth(percent: Float) = Utils.percentageInt(Gdx.graphics.width.toFloat(), percent).toFloat()

fun OrthographicCamera.moveToBottomLeft(x: Float, y: Float) {
    this.position.x = x + this.viewportWidth * this.zoom / 2f
    this.position.y = y + this.viewportHeight * this.zoom / 2f
}

/**
 * Automatically calls [Batch.begin] and [Batch.end].
 * @param action inlined. Executed after [Batch.begin] and before [Batch.end].
 */
inline fun <B : Batch> B.use(action: (B) -> Unit) {
    begin()
    action(this)
    end()
}

/**
 * Automatically calls [ShaderProgram.begin] and [ShaderProgram.end].
 * @param action inlined. Executed after [ShaderProgram.begin] and before [ShaderProgram.end].
 */
inline fun <S : ShaderProgram> S.use(action: (S) -> Unit) {
    begin()
    action(this)
    end()
}

inline fun <S : FrameBuffer> S.use(action: (S) -> Unit) {
    begin()
    action(this)
    end()
}

object Utils {
    //region NUMBER FORMAT
    val magnitudes = arrayOf("K", "M", "B", "T", "Qa", "Qi", "Sx", "Sp", "Oc", "No", "Dc", "UDc", "DDc",
            "TDc", "QaD", "QiD", "SxD", "SpD", "OcD", "NoD", "Vi")

    private fun doTheFormat(number: Double, decimals: Int): String {
        return String.format(Locale.US, "%,." + Integer.toString(decimals) + "f", number)
    }

    /**
     * Adapted from https://stackoverflow.com/a/30688774. Transforms input to Double and adds magnitudes to it. Should cover
     * all range of values of Long but not all from Double
     */
    fun formatNumber(number: Number, normalDecimals: Int = 0, magnitudeDecimals: Int = 2, ignoreThousandsMagnitude: Boolean = false,
                     magnitudes: kotlin.Array<String> = Utils.magnitudes): String
    {
        fun doesNotNeedMagnitude(number: Double, originalNumber: Boolean = true): Boolean {
            return number < 1000 || (number < 1_000_000 && ignoreThousandsMagnitude && originalNumber)
        }

        var thisNumber = number.toDouble()

        if (thisNumber <= -9200000000000000000L) {
            return "-9.2E"
        }

        if (doesNotNeedMagnitude(thisNumber)) return doTheFormat(thisNumber, normalDecimals)

        var i = 0
        while (true) {
//            if (thisNumber < 10000 && thisNumber % 1000 >= 100)
//                return ret + doTheFormat(thisNumber / 1000, magnitudeDecimals) +
//                        ',' + doTheFormat(thisNumber % 1000 / 100 + magnitudes[i].toDouble(), magnitudeDecimals)
            thisNumber /= 1000.0
            if (doesNotNeedMagnitude(thisNumber, false))
                return doTheFormat(thisNumber, magnitudeDecimals) + magnitudes[i]
            i++
        }
    }
    //endregion

    fun solidColorDrawable(color: Color): Drawable {
        return solidColorDrawable(color.r, color.g, color.b, color.a)
    }

    fun solidColorDrawable(color: Color, a: Float): Drawable {
        return solidColorDrawable(color.r, color.g, color.b, a)
    }

    fun solidColorDrawable(r: Float, g: Float, b: Float, a: Float): Drawable {
        return object : BaseDrawable() {
            override fun draw(batch: Batch?, x: Float, y: Float, width: Float, height: Float) {
                super.draw(batch, x, y, width, height)

                batch!!
                batch.setColor(r, g, b, a)
                batch.draw(Engine.pixelTexture, x, y, width, height)
                batch.color = Color.WHITE
            }
        }
    }

    /** Using VisUI, creates a slider which allows to change a value. Pass a function to easily update a variable with the slider's value
     * Uses ExtLabels with the default font */
    fun visUI_valueChangingSlider(valueName: String, minValue: Float = 0f, maxValue: Float = 1f, startingValue: Float = 0.5f,
                                  stepSize: Float = 0.01f, decimals: Int = 2, getSliderValue: (value: Float) -> Unit): ExtTable {
        return ExtTable().also {
            it.pad(10f)
            it.add(ExtLabel(valueName)).center()

            it.row()

            val visSlider = VisSlider(minValue, maxValue, stepSize, false).also {
                it.value = startingValue
            }

            fun format(number: Number): String {
                return formatNumber(number, decimals, 3, true)
            }

            it.add(Table().also {
                it.add(ExtLabel(format(minValue)))

                it.add(visSlider)

                it.add(ExtLabel(format(maxValue)))
            })

            it.row()
            it.add(ExtLabel().also {
                it.textUpdateFunction = { visSlider.value.toString() }
            }).center()

            it.updateFunction = {
                getSliderValue(visSlider.value)
            }
        }
    }

    /** VisUI checkbox, but the text is in a [ExtLabel] */
    fun visUI_customCheckBox(text: String, checked: Boolean, textPadLeft: Float = 1f): Table {
        return Table().also {
            it.add(VisCheckBox("", checked).also {
                it.add(ExtLabel(text).also { it.setAlignment(Align.left) }).padLeft(textPadLeft)
            })
        }
    }

    // not working
//    fun hasEmptyConstructor(kotlinClass: KClass<out Any>): Boolean {
//        for (c in kotlinClass.constructors) {
//            if (c.parameters.size == 2) {
//                return true
//            }
//        }
//
//        return false
//    }

    val dummyVector2 = Vector2()

    fun projectPosition(x: Number, y: Number, originViewport: Viewport? = null, endViewport: Viewport? = null, flipY: Boolean = true): Vector2 {
        val coords = dummyVector2
        val thisX = x.toFloat()
        val thisY = y.toFloat()
        coords.set(thisX, thisY)
        originViewport?.project(coords)
        endViewport.ifNotNull {
            if (flipY) coords.y = Gdx.graphics.height - coords.y // looks like you need to flip it in this case, probably because
                                                                    // the unproject requires "opengl-oriented y"
            it.unproject(coords)
        }

        if (originViewport?.screenWidth == 0) {
            com.dcostap.engine.printDebug("WARNING! - Projecting position of worldViewport with width 0. This will give unexpected results." +
                    "\nCheck you are not calling the method before resize() was called (viewport still has no size)")
        }

        return coords
    }

    fun clearScreen(r: Int = 58, g: Int = 68, b: Int = 102) {
        Gdx.gl.glClearColor(r / 255f, g / 255f, b / 255f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
    }

    fun scene2dActionShake(maximumMovingQuantity: Float, originalX: Float = 0f, originalY: Float = 0f): Action {
        fun random(): Float {
            return getRandomFloatInsideRange(0f, maximumMovingQuantity, true)
        }
        fun shake(): Action {
            return Actions.sequence(
                    Actions.moveBy(random(), random(), 0.03f, Interpolation.pow3Out),
                    Actions.moveTo(originalX, originalY, 0.02f, Interpolation.pow3Out))
        }

        return Actions.sequence(shake(),shake(),shake(),shake())
    }

    private val rand = Random()

    fun getDecimalPart(number: Float): Float {
        return number - number.toInt()
    }

    fun percentage(number: Number, percentage: Number): Float {
        return (number.toDouble() * (percentage.toDouble() / 100.0)).toFloat()
    }

    fun percentageInt(number: Number, percentage: Number): Int {
        return percentage(number, percentage).toInt()
    }

    fun removeExtensionFromFilename(filename: String): String {
        val i = filename.lastIndexOf(".")
        return if (i >= 0)
            filename.substring(0, i)
        else
            filename
    }

    fun getFileName(filename: String): String {
        val paths = filename.split("/".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        return paths[paths.size - 1]
    }

    /** Includes 0, doesn't include length (From 0 to length - 1)  */
    fun getRandomInteger(length: Int): Int {
        return rand.nextInt(length)
    }

    fun getRandomFloat(): Float {
        return rand.nextFloat()
    }

    /** Returns 1 or -1 */
    fun getRandomSign(): Int {
        return if (getRandomFloat() > 0.5) 1 else -1
    }

    fun clamp(value: Number, min: Number, max: Number): Float {
        val thisValue = value.toDouble()
        val thisMin = min.toDouble()
        val thisMax = max.toDouble()

        if (thisValue < thisMin) return thisMin.toFloat()
        return if (thisValue > thisMax) thisMax.toFloat() else thisValue.toFloat()
    }

    fun chance(percentageOfTrue: Number): Boolean {
        var thisPercentageOfTrue = percentageOfTrue.toFloat()
        thisPercentageOfTrue = clamp(thisPercentageOfTrue, 0f, 100f)
        return getRandomFloat() < thisPercentageOfTrue / 100f
    }

    /** Modifies input Array  */
    fun getRangeOfIntegers(minValue: Int, maxValue: Int, arrayToPopulate: Array<Int>) {
        if (minValue == maxValue) {
            return
        } else if (minValue > maxValue)
            throw IllegalArgumentException("getRangeOfIntegers, minValue: " + minValue
                    + "is bigger than maxValue: " + maxValue)

        for (i in minValue..maxValue) {
            arrayToPopulate.add(i)
        }
    }

    fun getRandomFloatInsideRange(positiveMin: Float, positiveMax: Float, randomSign: Boolean = false): Float {
        if (positiveMax == positiveMin) return positiveMax
        if (positiveMax < positiveMin)
            throw IllegalArgumentException("Invalid input floats: " + positiveMax + " isn't" +
                    "bigger than " + positiveMin)

        return mapToRange(getRandomFloat(), 0f, 1f, positiveMin, positiveMax) * if (randomSign) if (chance(50f)) 1 else -1 else 1
    }

    /** Clamps input values to input range, then maps them output range.
     *
     * inputLow must be lower than inputHigh, but outputLow can be bigger than outputHigh; map still will happen correctly  */
    fun mapToRange(inputNumber: Float, inputLow: Float, inputHigh: Float, outputLow: Float, outputHigh: Float): Float {
        var thisOutputLow = outputLow
        var thisOutputHigh = outputHigh
        if (inputNumber < inputLow) return thisOutputLow
        if (inputNumber > inputHigh) return thisOutputHigh

        var switched = false
        if (thisOutputLow > thisOutputHigh) {
            val temp = thisOutputHigh
            thisOutputHigh = thisOutputLow
            thisOutputLow = temp
            switched = true
        }

        val scale = (thisOutputHigh - thisOutputLow) / (inputHigh - inputLow)
        val value = (inputNumber - inputLow) * scale + thisOutputLow

        return if (switched) {
            thisOutputLow - value + thisOutputHigh
        } else value
    }

    fun lerp(point1: Float, point2: Float, alpha: Float): Float {
        return point1 + alpha * (point2 - point1)
    }

    fun getXFromDirectionMovement(rawSpeed: Float, directionDegrees: Float): Float {
        var thisDirectionDegrees = directionDegrees
        if (thisDirectionDegrees < 0) {
            thisDirectionDegrees += 360f
        }
        return rawSpeed * Math.cos(Math.toRadians(thisDirectionDegrees.toDouble())).toFloat()
    }

    fun getYFromDirectionMovement(rawSpeed: Float, directionDegrees: Float): Float {
        var thisDirectionDegrees = directionDegrees
        if (thisDirectionDegrees < 0) {
            thisDirectionDegrees += 360f
        }
        return rawSpeed * Math.sin(Math.toRadians(thisDirectionDegrees.toDouble())).toFloat()
    }

    /**
     * Length (angular) of a shortest way between two angles.
     * It will be in range [-180, 180] (signed).
     */
    fun getAngleDifferenceSigned(sourceAngle: Float, targetAngle: Float): Float {
        var thisSourceAngle = sourceAngle
        var thisTargetAngle = targetAngle
        thisSourceAngle = Math.toRadians(thisSourceAngle.toDouble()).toFloat()
        thisTargetAngle = Math.toRadians(thisTargetAngle.toDouble()).toFloat()
        return Math.toDegrees(Math.atan2(Math.sin((thisTargetAngle - thisSourceAngle).toDouble()), Math.cos((thisTargetAngle - thisSourceAngle).toDouble()))).toFloat()
    }

    fun getAngleDifferenceNotSigned(sourceAngle: Float, targetAngle: Float): Float {
        return Math.abs(getAngleDifferenceSigned(sourceAngle, targetAngle))
    }

    fun getAngleBetweenPoints(start: Vector2, end: Vector2): Float {
        var angle = Math.toDegrees(Math.atan2((end.y - start.y).toDouble(), (end.x - start.x).toDouble())).toFloat()

        if (angle < 0) {
            angle += 360f
        }

        return angle
    }

    fun getAngleBetweenPoints(x1: Float, y1: Float, x2: Float, y2: Float): Float {
        var angle = Math.toDegrees(Math.atan2((y2 - y1).toDouble(), (x2 - x1).toDouble())).toFloat()

        if (angle < 0) {
            angle += 360f
        }

        return angle
    }

    fun getDistanceBetweenPoints(x1: Float, y1: Float, x2: Float, y2: Float): Float {
        return Math.hypot((x1 - x2).toDouble(), (y1 - y2).toDouble()).toFloat()
    }

    fun getDistanceBetweenPoints(point1: Vector2, point2: Vector2): Float {
        return getDistanceBetweenPoints(point1.x, point1.y, point2.x, point2.y)
    }

    fun getColorFrom255RGB(red: Int, green: Int, blue: Int, alpha: Float = 1f): Color {
        return Color(red / 255f, green / 255f, blue / 255f, alpha)
    }

    fun drawProgressBar(gameDrawer: GameDrawer, x: Float, y: Float, width: Float, height: Float,
                        value: Float, maxValue: Float, minValue: Float, background: Color, foreGround: Color) {
        gameDrawer.color = background
        gameDrawer.drawRectangle(x, y, width, height, 0f, true)

        gameDrawer.color = foreGround
        gameDrawer.drawRectangle(x, y, mapToRange(value, minValue, maxValue, 0f, width), height, 0f, true)

        gameDrawer.resetColor()
    }

    fun getClosestNumberInList(number: Float, list: FloatArray): Float {
        var ret = list[0]
        var diff = Math.abs(ret - number)
        for (i in 1 until list.size) {
            if (ret != list[i]) {
                val newDiff = Math.abs(list[i] - number)
                if (newDiff < diff) {
                    ret = list[i]
                    diff = newDiff
                }
            }
        }
        return ret
    }

    fun growRectangle(rectangle: Rectangle, growth: Float): Rectangle {
        rectangle.x -= growth
        rectangle.y -= growth
        rectangle.width += growth * 2
        rectangle.height += growth * 2
        return rectangle
    }
}
