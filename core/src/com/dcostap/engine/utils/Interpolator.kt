package com.dcostap.engine.utils

import com.badlogic.gdx.math.Interpolation

/**
 * Created by Darius on 10/04/2018.
 */
class Interpolator(var interpolation: Interpolation, var duration: Float, var startValue: Float, var endValue: Float) : Updatable {
    val timer = Timer(10000000f)

    fun getValue(): Float {
        val value = interpolation.apply(Utils.mapToRange(timer.elapsed, 0f, duration, 0f, 1f))
        return Utils.mapToRange(value, 0f, 1f, startValue, endValue)
    }

    override fun update(delta: Float) {
        timer.tick(delta)
    }

    fun resetElapsed() {
        timer.reset()
    }

    val hasFinished
        get() = timer.elapsed > duration
}
