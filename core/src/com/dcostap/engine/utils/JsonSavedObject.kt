package com.dcostap.engine.utils

import com.badlogic.gdx.utils.JsonValue

/** Created by Darius on 19-Jul-18.
 *
 * A [JsonValue.ValueType.object] which allows to automatically save and load variables without
 * specifying a name: they are stored in order by the [counter], so you save and load
 * the variables on the same order for it to work. */
class JsonSavedObject : JsonValue(JsonValue.ValueType.`object`) {
    var counter: Int = 0

    fun saveValues(vararg values: Any) {
        for (v in values) {
            saveAnotherValue(v)
        }
    }

    fun saveAnotherValue(value: Any) {
        try {
            addChildValue(counter.toString(), value)
        } catch (exc: Exception) {
            com.dcostap.engine.printDebug("(JsonSavedObject) Error when saving value: $value. \nException: ${exc.message}" +
                    "\nIgnored and continuing...")
        }
        counter++
    }

    fun <T : Any> loadAnotherValue(defaultValue: T): T {
        return getChildValue(counter.toString(), defaultValue) as T
    }
}