package com.dcostap.engine.utils.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.utils.viewport.ExtendViewport
import com.badlogic.gdx.utils.viewport.Viewport
import com.dcostap.engine.Engine
import com.dcostap.engine.utils.Drawable
import com.dcostap.engine.utils.GameDrawer
import com.dcostap.engine.utils.Updatable
import com.dcostap.engine.utils.input.InputController
import com.dcostap.engine.utils.input.InputListener

/**
 * Convenience behavior already coded: base variables with Engine, a Camera, a Viewport, InputController and GameDrawer
 *
 * Extend this class instead of ScreenAdapter if you need all that functionality
 */
abstract class BaseScreen(engine : Engine) : Screen, Drawable, Updatable, InputListener {
    val camera: OrthographicCamera = OrthographicCamera()
    var worldViewport: Viewport
        protected set

    val gameDrawer: GameDrawer

    val inputController: InputController
    init {
        setupPPM()
        worldViewport = createViewport()
        gameDrawer = GameDrawer(engine.batch)

        inputController = InputController(worldViewport)
        Gdx.input.inputProcessor = InputMultiplexer(Engine.debugStage, inputController)

        inputController.registerObserver(this)
    }

    /** If the screen uses a custom PPM setting, adjust it here */
    open fun setupPPM() {

    }

    override fun hide() {

    }

    override fun show() {

    }

    /** Override to change created viewport */
    open fun createViewport(): Viewport {
        return ExtendViewport(Engine.BASE_WIDTH / Engine.PPM_WIDTH.toFloat(),
                Engine.BASE_HEIGHT / Engine.PPM_HEIGHT.toFloat(), camera)
    }

    abstract override fun draw(gameDrawer: GameDrawer, delta: Float)

    private var firstUpdate = true

    /** Called on the first frame update() method is called */
    fun firstUpdate(delta: Float) {

    }

    override fun update(delta: Float) {
        if (firstUpdate) {
            firstUpdate = false
            firstUpdate(delta)
        }

        // round camera position to 3 decimals, to avoid random blank lines between map tiles
        camera.position.x = (Math.round(camera.position.x * 1000.0) / 1000.0).toFloat()
        camera.position.y = (Math.round(camera.position.y * 1000.0) / 1000.0).toFloat()

        inputController.update(delta)
    }

    override fun render(delta: Float) {
        update(delta)
        draw(gameDrawer, delta)
    }

    override fun resize(width: Int, height: Int) {
        worldViewport.update(width, height)
    }

    override fun pause() {}

    override fun resume() {}

    override fun dispose() {
        inputController.removeObserver(this)
    }

    override fun touchDownEvent(screenX: Float, screenY: Float, worldX: Float, worldY: Float, pointer: Int, isJustPressed: Boolean) {

    }

    override fun touchReleasedEvent(screenX: Float, screenY: Float, worldX: Float, worldY: Float, button: Int, pointer: Int) {

    }

    companion object {

    }
}
