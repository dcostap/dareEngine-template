package com.dcostap.engine.utils.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.utils.viewport.ScreenViewport
import com.dcostap.engine.Engine
import com.dcostap.engine.ui.UIController
import com.dcostap.engine.utils.input.InputController

/**
 * Created by Darius on 28/12/2017.
 *
 * Adds base UI functionality: UIController and a Stage
 */
abstract class BaseScreenWithUI(engine : Engine, skin: Skin) : BaseScreen(engine) {
    var stage: Stage
    val uiController: UIController

    val stageInputController: InputController
    init {
        stage = createStage()

        uiController = UIController(skin, stage)
        stageInputController = InputController(stage.viewport)

        Gdx.input.inputProcessor = InputMultiplexer(Engine.debugStage, stage, stageInputController, inputController)
    }

    override fun update(delta: Float) {
        super.update(delta)

        stageInputController.update(delta)
    }

    /** Override to change created Stage */
    open fun createStage(): Stage {
        return Stage(ScreenViewport())
    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)
        stage.viewport.update(width, height, true)
    }

    override fun dispose() {
        stage.dispose()
    }
}
