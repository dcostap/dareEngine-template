package com.dcostap.engine.map.entities

import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.utils.Json
import com.badlogic.gdx.utils.JsonValue
import com.dcostap.engine.Engine
import com.dcostap.engine.utils.JsonSavedObject
import com.dcostap.engine.utils.Saveable
import com.dcostap.engine.utils.addChildValue

/**
 * Created by Darius on 21/03/2018.
 *
 * Holds a Rectangle which represents the absolutely-positioned BoundingBox. The real information is
 * in the internal [.relativeBoundingBox], which specifies the BB's width, height and offset.
 * Modifying the relative BB (the only way to modify the actual BB) raises
 * exception if it is marked as [.isStatic].
 *
 * When first creating the BoundingBox, set the size with the methods, then set the [.isStatic] flag later (this
 * avoids raising exceptions while building the BB)
 */
open class BoundingBox(val entity: Entity, @Transient val name: String) : Saveable {
    val relativeBoundingBox = Rectangle(0f, 0f, 0f, 0f)
    private val internalAbsoluteBoundingBox = Rectangle(0f, 0f, 0f, 0f)

    val isStatic get() = entity.isStatic

    fun modifyRelativeBoundingBox(offsetX: Float, offsetY: Float, width: Float, height: Float) {
        checkIfAllowedToModify()
        relativeBoundingBox.set(offsetX, offsetY, width, height)
        adjustBoundingBoxSize()
        updateAbsoluteBoundingBox()
    }

    fun modifyRelativeBoundingBox(rectangle: Rectangle) {
        this.modifyRelativeBoundingBox(rectangle.x, rectangle.y, rectangle.width, rectangle.height)
    }

    fun modifyRelativeBoundingBoxPixels(offsetX: Number, offsetY: Number, width: Number, height: Number) {
        this.modifyRelativeBoundingBox(Engine.pixelsToUnitsWidth(offsetX.toFloat()), Engine.pixelsToUnitsWidth(offsetY.toFloat()),
                Engine.pixelsToUnitsWidth(width.toFloat()), Engine.pixelsToUnitsWidth(height.toFloat()))
    }

    private fun updateAbsoluteBoundingBox() {
        internalAbsoluteBoundingBox.x = entity.x + relativeBoundingBox.x
        internalAbsoluteBoundingBox.y = entity.y + relativeBoundingBox.y
        internalAbsoluteBoundingBox.width = relativeBoundingBox.width
        internalAbsoluteBoundingBox.height = relativeBoundingBox.height
    }

    /** BoundingBox size is reduced a bit to avoid BBs with size of 1 unit to occupy 2 cells, when the BB is snapped to the grid
     *
     *  (because when checking for occupied cells, if BB is at position 2, 2 with size of 1, 1; the algorithm will
     * return cells 2, 2 and (2 + 1), (2 + 1) as occupied. But BB of size 1 is normally meant to occupy 1 cell only; this
     * fixes it)  */
    private fun adjustBoundingBoxSize() {
        if (relativeBoundingBox.width == 0f && relativeBoundingBox.height == 0f) return  // don't do it if size is 0
        val bbSizeMargin = 0.01f
        relativeBoundingBox.width -= bbSizeMargin
        relativeBoundingBox.height -= bbSizeMargin
    }

    private fun checkIfAllowedToModify() {
        if (isStatic && entity.addedToMap)
            throw UnsupportedOperationException("Tried to modify bounding box of static Entity: " + javaClass.simpleName
                    + "\nThis is unsupported as it leads to bugs")
    }

    fun getAbsoluteBoundingBox(): Rectangle {
        updateAbsoluteBoundingBox()
        return internalAbsoluteBoundingBox
    }

    override fun save(): JsonValue {
        val json = Json()
        return JsonSavedObject().also {
            it.addChildValue("name", name)
            it.addChildValue("relativeBoundingBox", json.toJson(relativeBoundingBox))
            it.addChildValue("internalAbsoluteBoundingBox", json.toJson(internalAbsoluteBoundingBox))
        }
    }

    fun load(info: JsonValue) {
        val json = Json()
        relativeBoundingBox.set(json.fromJson(Rectangle::class.java, info.getString("relativeBoundingBox")))
        internalAbsoluteBoundingBox.set(json.fromJson(Rectangle::class.java, info.getString("internalAbsoluteBoundingBox")))
    }
}
