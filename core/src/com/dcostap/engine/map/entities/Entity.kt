package com.dcostap.engine.map.entities

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.GridPoint2
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.*
import com.badlogic.gdx.utils.Array
import com.dcostap.engine.Engine
import com.dcostap.engine.map.DrawableAndSortable
import com.dcostap.engine.map.EntityTiledMap
import com.dcostap.engine.map.MapCell
import com.dcostap.engine.map.map_loading.CustomProperties
import com.dcostap.engine.ui.utils.ExtLabel
import com.dcostap.engine.ui.utils.ExtTable
import com.dcostap.engine.ui.utils.ResizableActorTable
import com.dcostap.engine.utils.*
import com.dcostap.engine.utils.actions.ActionsUpdater
import com.dcostap.engine.utils.input.InputController
import com.kotcrab.vis.ui.VisUI
import com.kotcrab.vis.ui.widget.CollapsibleWidget
import com.kotcrab.vis.ui.widget.spinner.IntSpinnerModel
import com.kotcrab.vis.ui.widget.spinner.Spinner

/**
 * Created by Darius on 14/09/2017.
 *
 * Entities are instances of the game world, stored and updated on a [EntityTiledMap].
 *
 * Default constructor creates a non-solid non-static Entity positioned at 0, 0 with boundingBox of size 0
 *
 * BoundingBox created in the constructor is the default BB; Entities may have more than one, each one identified by a
 * name (default BB's name is "default").
 *
 * Any BB may be modified later after creation (use [getActualBoundingBox]), however if the Entity is
 * static any modification will throw an Exception unless it is done before [addedToMap] (map will actually
 * add the Entity in the next frame)
 *
 * Position modification on static entities is forbidden as well in the same way
 *
 * Static entities:
 * - can't move from initial position.
 * - will be included in map's quadTree as a static entity, with information which won't change until it is removed;
 * they are more efficient
 * - MapCells occupied will save information from the Entity (unless [getsTiledToCell] is false)
 *
 * Dynamic entities:
 * - can move
 * - will be included in map's quadTree as a dynamic entity, which means it will be updated each frame on the quadTree
 * whenever it moves. Dynamic entity not moving is practically the same as a static entity in terms of performance.
 *
 * @param boundingBox     x, y values are the offset of the bounding box. All in game units.
 * @param isSolid         Flag that may be used in collision response. Generally means that solid entities should not
 * be overlapped.
 * @param getsTiledToCell Only applies to static entities. If true, entity will be included in a list in overlapped cells.
 */
abstract class Entity @JvmOverloads constructor(val position: Vector2 = Vector2(), boundingBox: Rectangle = Rectangle(),
                                                var isSolid: Boolean = false, var isStatic: Boolean = false,
                                                var getsTiledToCell: Boolean = true)
    : Updatable, DrawableAndSortable
{
    val worldViewport get() = map.worldViewport

    /** Higher depth = further away from the camera */
    var depth = 0

    var addedToMap = false
    @Transient lateinit var map: EntityTiledMap
    var actions = ActionsUpdater()
        private set

    fun getsTiledToCell(): Boolean = isStatic && getsTiledToCell

    var tiledEditorGroupNames: Array<String>? = null
    var customProperties = CustomProperties()

    var isKilled = false
        private set
    private var hasMoved = false

    /** Update the collision state if the Entity moved, to update the Array */
    @Transient val possibleCollidingEntities = Array<Entity>()

    val debugEntityFlashingThing = FlashingThing()

    @Transient private val dummyEntityArray = Array<Entity>()

    var allBoundingBoxes: ObjectMap<String, BoundingBox>
        protected set

    /** @return The **default** absolute positioned Rectangle that represents the bounding box */
    val boundingBox: Rectangle
        get() = getBoundingBox("default")

    private val debugTable = ResizableActorTable()
    private val debugPopUpTable = ResizableActorTable()

    var debugTableVisible = false
    var debugPopUpTableVisible = false

    val debugCollapsibleWidget: CollapsibleWidget

    private var initDebug = true

    init {
        allBoundingBoxes = ObjectMap()
        addBoundingBox("default", boundingBox)

        // debug UI table
        ExtLabel.defaultFont = VisUI.getSkin().getFont("small-font")
        ExtLabel.defaultColor = Color.WHITE

        debugCollapsibleWidget = CollapsibleWidget()

        debugTable.add(Table().also {
            it.align(Align.top)
            it.top()
            it.background = Utils.solidColorDrawable(0f, 0f, 0f, 0.53f)
            it.pad(4f)

            it.add(ExtTable().also {
                it.top()
                it.add(ExtLabel(this.javaClass.simpleName))
                it.add(Utils.visUI_customCheckBox("Show", false).also {
                    it.addChangeListener {
                        debugCollapsibleWidget.isCollapsed = !debugCollapsibleWidget.isCollapsed
                    }
                }).padLeft(8f)
            })

            it.row()

            debugCollapsibleWidget.isCollapsed = true
            it.add(debugCollapsibleWidget)
        }).top()

        debugTable.isVisible = false
        debugPopUpTable.isVisible = false

        Engine.debugStage.addActor(debugTable)
        Engine.debugStage.addActor(debugPopUpTable)
    }

    /** Called once when added to the map. Before this [map] might be un-initialized */
    open fun justAddedToMap() {
        addedToMap = true

        previousPosition.set(position)
    }

    open fun justRemovedFromMap() {
        addedToMap = false
    }

    open fun createDebugInfoTable(contents: Table) {
        contents.add(ExtLabel(textUpdateDelay = 0.15f) {
            "x: ${Utils.formatNumber(x, 3)}; y: " + Utils.formatNumber(y, 3)
        })
        contents.row()
        contents.add(ExtLabel().also {
            it.textUpdateFunction = {"solid: $isSolid"}
        })
        contents.row()
        contents.add(ExtLabel().also {
            it.textUpdateFunction = {"static: $isStatic"}
        })
        contents.row()

        contents.add(Table().also {
            it.add(ExtLabel().also {
                it.textUpdateFunction = {"depth: $depth"}
            })
//            it.add(ExtLabel().also {
//                it.textUpdateFunction = {"depth: $depth"}
//            })
            val spinner = IntSpinnerModel(depth, -100000, 100000, 1)
            it.add(Spinner("", spinner).also {
                    it.addChangeListener {
                        depth = spinner.value
                    }
            }).padLeft(10f)
        })
    }

    /** This table only appears when the mouse hovers over the default bounding box */
    open fun createDebugPopUpTable(contents: Table) {
        contents.align(Align.top)
        contents.top()
        contents.background = Utils.solidColorDrawable(0f, 0f, 0f, 0.53f)
        contents.pad(4f)

        contents.add(ExtTable().also {
            it.top()
            it.add(ExtLabel(this.javaClass.simpleName))
            it.row()
            it.add(ExtLabel(textUpdateDelay = 0.15f) {
                "x: ${Utils.formatNumber(x, 3)}; y: " + Utils.formatNumber(y, 3) + "; depth: $depth"
            })
        })
        contents.row()
    }

    /**
     * If relative and using delta, will move "units specified" /s. To instantly move the quantity specified pass a
     * delta value of 1
     *
     * @param relative If not relative, delta is ignored
     * @param delta    Use value 1 to ignore delta and move the exact quantity specified
     */
    fun move(x: Float, y: Float, relative: Boolean, delta: Float = 1f) {
        var thisX = x
        var thisY = y

        if (relative) {
            thisX *= delta
            thisY *= delta

            position.x += thisX
            position.y += thisY
        } else {
            position.x = thisX
            position.y = thisY
        }
    }

    /**
     * "killed" entities must be completely ignored and removed from lists so that GC can remove the instance.
     */
    open fun remove() {
        debugTable.remove()
        debugPopUpTable.remove()
        isKilled = true
        map.removeEntity(this)
    }

    private val previousPosition = Vector2(position)

    override fun update(delta: Float) {
        if (addedToMap && !previousPosition.equals(position)) {
            hasMoved = true

            if (isStatic)
                throw UnsupportedOperationException("Moved a static entity which is already added to a map: " + javaClass.simpleName)
        }

        if (initDebug) {
            initDebug = false

            val table = Table()
            debugCollapsibleWidget.setTable(table)
            table.defaults().left()
            createDebugInfoTable(table)
            debugPopUpTable.add(Table().also {
                createDebugPopUpTable(it)
            })
        }

        previousPosition.set(position)

        actions.update(delta)

        debugTable.isVisible = !(!Engine.DEBUG_STAGE_UI || !Engine.DEBUG_STAGE_UI_ENTITY_INFO || !debugTableVisible)

        map.ifNotNull {
            val coords = Pools.obtain(Vector2::class.java)
            coords.set(Utils.projectPosition(x, y - 0.5f, map.worldViewport, Engine.debugStage.viewport))
            debugTable.setPosition(coords.x, coords.y - debugTable.height / 2f)

            Pools.free(coords)
        }
    }

    override fun draw(gameDrawer: GameDrawer, delta: Float) {
        if (Engine.DEBUG && Engine.DEBUG_ENTITIES)
            drawDebug(gameDrawer, delta)
    }

    private val debugRect = Rectangle()

    private fun drawDebug(gameDrawer: GameDrawer, delta: Float) {
        gameDrawer.alpha = Engine.DEBUG_TRANSPARENCY
        gameDrawer.color = if (isStatic) Color.RED else Color.BLUE
        for (bb in allBoundingBoxes.values()) {
            gameDrawer.drawRectangle(bb.getAbsoluteBoundingBox(), Engine.DEBUG_LINE_THICKNESS, false)

            if (isSolid) {
                debugRect.set(bb.getAbsoluteBoundingBox())
                Utils.growRectangle(debugRect, -Engine.DEBUG_LINE_THICKNESS * 1.6f)
                gameDrawer.drawRectangle(debugRect, Engine.DEBUG_LINE_THICKNESS, false)
            }
        }

        gameDrawer.color = Color.CHARTREUSE
        gameDrawer.alpha = 0.65f
        gameDrawer.drawCross(x, y, 0.2f, Engine.DEBUG_LINE_THICKNESS * 0.9f)

        gameDrawer.resetAlpha()
        gameDrawer.resetColor()

        debugEntityFlashingThing.update(delta)

        if (debugEntityFlashingThing.isFlashing) {
            debugEntityFlashingThing.setupGameDrawer(gameDrawer)

            gameDrawer.drawRectangle(boundingBox, 0f, true)

            debugEntityFlashingThing.resetGameDrawer(gameDrawer)
        }
    }

    /** @param name Identifier of the bounding box, by default the **default** BB
     * @return The absolute positioned Rectangle that represents the bounding box */
    fun getBoundingBox(name: String = "default"): Rectangle {
        return allBoundingBoxes.get(name).getAbsoluteBoundingBox()
    }

    fun addBoundingBox(name: String, rectangle: Rectangle): BoundingBox {
        val newBB = BoundingBox(this, name)
        newBB.modifyRelativeBoundingBox(rectangle)
        allBoundingBoxes.put(name, newBB)
        return newBB
    }

    fun removeBoundingBox(name: String) {
        allBoundingBoxes.remove(name)
    }

    fun getActualBoundingBox(name: String = "default"): BoundingBox {
        return allBoundingBoxes.get(name)
    }

    /**
     * Call each frame to update the list of possible colliding entities. Uses the **default bounding box** to check.
     *
     * Call this method when:
     *  * entities might have been added / removed
     *  * when dynamic entities, including this one, might have moved
     */
    fun updateCollidingState(includeDynamicEntities: Boolean, boundingBoxName: String = "default") {
        possibleCollidingEntities.clear()

        for (entity in map.collisionTree.getPossibleCollidingEntities(getBoundingBox(boundingBoxName), includeDynamicEntities)) {
            if (entity === this) continue
            possibleCollidingEntities.add(entity)
        }
    }

    /** uses default bounding box  */
    fun isCollidingWithSpecificEntity(otherEntity: Entity): Boolean {
        return boundingBox.overlaps(otherEntity.boundingBox)
    }

    /**
     * Use this to check for solid collision without having to do the expensive call to the CollisionTree,
     * if the Entity will only collide with the boundaries of the map cells
     *
     *
     * Limitations compared to checking collision against Entities (using [.updateCollidingState]):
     *  * Solid collision is only done against mapCell's boundaries, no precise collision checking on arbitrary positions
     *  * Only affected by (solid & tiled) Entities (which modify the mapCells) or cells directly marked as solid
     *
     *  @see [MapCell.isSolid]
     */
    fun isCollidingWithSolidMapCell(): Boolean {
        for (mapCell in map.getCellsOccupiedByRectangle(boundingBox)) {
            if (mapCell.isSolid) {
                return true
            }
        }

        return false
    }

    fun getOverlappedMapCells(): Array<MapCell> {
        return map.getCellsOccupiedByRectangle(boundingBox)
    }

    /**
     * Uses array of possible colliding Entities, so it may be outdated if updateCollidingState wasn't called since
     * the Entity last moved - or since any other Entity moved
     *
     *
     * Don't keep references to returned Array!
     *
     * @return All Entities found that are of the specified class and that collide with this Entity
     */
    fun isCollidingWithAnyEntityOfClass(vClass: Class<*>): Array<*> {
        dummyEntityArray.clear()

        for (entity in possibleCollidingEntities) {
            if (vClass.isInstance(entity) && isCollidingWithSpecificEntity(entity)) {
                dummyEntityArray.add(entity)
            }
        }

        return dummyEntityArray
    }

    fun hasMoved(): Boolean {
        return hasMoved
    }

    /**
     * Call on all Entities when a new update loop starts, since this variable is meant to show if the Entity
     * has moved since the loop started
     */
    fun resetHasMoved() {
        hasMoved = false
    }

    val x: Float
        get() = position.x

    val y: Float
        get() = position.y

    fun getTiledPosition(vectorToModify: Vector2): Vector2 {
        return vectorToModify.set(tiledX.toFloat(), tiledY.toFloat())
    }

    fun getTiledPosition(gridPointToModify: GridPoint2): GridPoint2 {
        return gridPointToModify.set(tiledX, tiledY)
    }

    val tiledX: Int
        get() = Math.floor(position.x.toDouble()).toInt()

    val tiledY: Int
        get() = Math.floor(position.y.toDouble()).toInt()

    /**
     * Entities can specify whether they are "valid" or not. Override on subclasses if needed.
     * AI algorithms should avoid interacting with invalid entities
     */
    open fun isValid(): Boolean = !isKilled

    override fun toString(): String {
        return (javaClass.simpleName + "(" + (if (isSolid) "solid" else "notSolid") + ", "
        + (if (isStatic) "static" else "notStatic") + ", " + (if (isKilled) "killed" else "notKilled") + ")"
        + "; hash: " + hashCode())
    }

    /** Still not implemented: Saving Actions in [ActionsUpdater] */
    open fun save(onlySaveProperties: Boolean = false): JsonValue {
        val json = JsonSavedObject()
        val jsonlibgdx = Json()

        if (!onlySaveProperties) {
            json.addChildValue("class", this.javaClass.name)

            json.addChildValue("bbs", JsonSavedObject().also {
                for (bb in allBoundingBoxes.values()) {
                    it.addChild("bb", bb.save())
                }
            })

            json.addChildValue("isStatic", isStatic)
            json.addChildValue("isSolid", isSolid)
            json.addChildValue("getsTiledToCell", getsTiledToCell)
            json.addChildValue("depth", depth)

//        json.addChildValue("actions", gson.toJson(actions))
            if (tiledEditorGroupNames != null) {
                json.addChildValue("tiledEditorGroupNames", jsonlibgdx.toJson(tiledEditorGroupNames!!.toArray()))
            }

            json.addChildValue("position", jsonlibgdx.toJson(position))

            json.addChildValue("isKilled", isKilled)
        }

        json.addChildValue("customProperties", jsonlibgdx.toJson(customProperties))

        return json
    }

    open fun load(jsonSavedObject: JsonValue) {
        val jsonlibgdx = Json()

        allBoundingBoxes.clear()

        for (bb in jsonSavedObject.get("bbs")) {
            val name = bb.getString("name")
            allBoundingBoxes.put(name, BoundingBox(this, name).also {
                it.load(bb)
            })
        }

        isStatic = jsonSavedObject.getBoolean("isStatic")
        isSolid = jsonSavedObject.getBoolean("isSolid")
        isKilled = jsonSavedObject.getBoolean("isKilled")
        getsTiledToCell = jsonSavedObject.getBoolean("getsTiledToCell")
        depth = jsonSavedObject.getInt("depth")

//        actions = gson.fromJson(jsonSavedObject.getString("actions"), ActionsUpdater::class.java)

        if (jsonSavedObject.has("tiledEditorGroupNames")) {
            tiledEditorGroupNames = Array(jsonlibgdx.fromJson(kotlin.Array<String>::class.java,
                    jsonSavedObject.getString("tiledEditorGroupNames")) as kotlin.Array<String>)
        }
        customProperties = jsonlibgdx.fromJson(CustomProperties::class.java, jsonSavedObject.getString("customProperties"))

        position.set(jsonlibgdx.fromJson(Vector2::class.java, jsonSavedObject.getString("position")))
    }

    override fun getDrawingRepresentativeY(): Float {
        return boundingBox.y
    }

    override fun getDrawingRepresentativeDepth(): Int {
        return depth
    }

    /** Useful function for correctly handling touch input in a [EntityTiledMap]
     * @see [DrawableAndSortable.handleTouchInput] */
    override fun handleTouchInput(inputController: InputController, stageInputController: InputController?): Boolean {
        debugPopUpTable.isVisible = false
        if (Engine.DEBUG_STAGE_UI && Engine.DEBUG_STAGE_UI_ENTITY_POPUP_INFO && debugPopUpTableVisible) {
            if (boundingBox.contains(inputController.mousePos.worldX, inputController.mousePos.worldY)) {
                debugPopUpTable.isVisible = true

                stageInputController.ifNotNull {
                    debugPopUpTable.setPosition(it.mousePos.worldX, it.mousePos.worldY)
                }.ifNull {
                    val mouse = Utils.projectPosition(Gdx.input.x.toFloat(), Gdx.graphics.height - Gdx.input.y.toFloat(), null, worldViewport)
                    debugPopUpTable.setPosition(mouse.x, mouse.y)
                }
            }
        }

        return false
    }
}
