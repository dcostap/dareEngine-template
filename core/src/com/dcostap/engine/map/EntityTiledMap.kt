package com.dcostap.engine.map

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.GridPoint2
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.utils.*
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.viewport.Viewport
import com.dcostap.engine.Engine
import com.dcostap.engine.map.entities.Entity
import com.dcostap.engine.map.map_loading.CustomProperties
import com.dcostap.engine.map.map_loading.EntityLoaderFromClass
import com.dcostap.engine.utils.*
import com.dcostap.engine.utils.input.InputController
import java.util.*


/**
 * Created by Darius on 14/09/2017.
 *
 * After creating the map object, call [initMap] to load basic variables
 *
 * When drawing, Entities and Tiles will be sorted according to their depth or y variables. (see [DrawableAndSortable])
 * Each mapCell may have more than one Tile. See [MapCell.tiles] for info on how tiles (graphical info on each cell) work
 *
 * For saving map info there are two approaches (you can choose what to save in [save], also see [load]):
 * - Raw save: save everything: all Entities, cells, properties, etc the way they are when doing the saving. Useful for complex maps with
 * many arbitrary Entities
 * - Custom Properties save: (from a json file) ignore Entities and cells, and only save properties in [save].
 * When you load the map, using JsonMapLoader, pass the previously saved info to its JsonMapLoader.
 * Useful for pre-designed maps (with Tiled for example), and maps that
 * don't change a lot (or changes are scripted, see JsonMapLoader and the interaction with groups of layers in Tiled).
 *
 * @param inputController If not null, will be used to call [DrawableAndSortable.handleTouchInput] on Entities and Cells
 * @param entityDeactivationBorder    In world map units, border applied to each edge of the camera view to
 * form the deactivation Rectangle. Entities outside that rectangle will not be updated
 */
open class EntityTiledMap (val engine: Engine, val worldViewport: Viewport,
                           val inputController: InputController? = null,
                           val stageInputController: InputController? = null,
                           private val deactivateEntities: Boolean = false,
                           private val entityDeactivationBorder: Int = 0)
    : Drawable, Updatable, Disposable
{
    val entityList = Array<Entity>()
    private val dynamicEntityList = Array<Entity>()
    private val toBeRemoved = Array<Entity>()
    private val toBeAdded = Array<Entity>()

    var customProperties = CustomProperties()

    private val drawingCameraBoundsBorder = 2
    private lateinit var mapCells: ObjectMap<GridPoint2, MapCell>
    var width: Int = 0
        private set
    var height: Int = 0
        private set
    lateinit var collisionTree: CollisionTree
        private set

    private val dummyGridPoint = GridPoint2()
    private val dummyCellArray = Array<MapCell>()
    private val dummyEntitySet = Array<Entity>()

    private val cameraRectangle = Rectangle()
    private val deactivationRectangle = Rectangle()
    private val mapCellArray = Array<MapCell>()

    var isInitiated = false
        private set

    private val worldCamera: OrthographicCamera = worldViewport.camera as OrthographicCamera

    /** Call after creating map! */
    open fun initMap(mapWidth: Int, mapHeight: Int, collisionTreeCellSize: Int) {
        if (isInitiated)
            throw RuntimeException("Tried to initiate a map that was already initiated.")

        mapCells = ObjectMap(mapWidth * mapHeight)

        for (x in 0 until mapWidth) {
            for (y in 0 until mapHeight) {
                mapCells.put(GridPoint2(x, y), MapCell(x, y, 1, this))
            }
        }

        this.width = mapWidth
        this.height = mapHeight

        this.collisionTree = CollisionTree(collisionTreeCellSize, mapWidth, mapHeight, this)

        isInitiated = true
    }

    /** Don't hold references of the returned Array, as it is reused inside this class */
    open fun getMapCells(): Array<MapCell> {
        mapCellArray.clear()
        return mapCells.values().toArray(mapCellArray)
    }

    open fun getMapCell(position: GridPoint2): MapCell {
        if (!isInsideMap(position.x.toFloat(), position.y.toFloat()))
            throw IllegalArgumentException("cell position: $position is outside of map!")
        return mapCells.get(position)
    }

    open fun getMapCell(x: Int, y: Int): MapCell {
        return getMapCell(dummyGridPoint.set(x, y))
    }

    private var wasUpdated = false

    override fun update(delta: Float) {
        wasUpdated = true

        updateCameraRectangle()
        removeAndAddEntities()

        collisionTree.resetDynamicEntities(dynamicEntityList)

        dummyEntitySet.clear()
        if (deactivateEntities)
            dummyEntitySet.addAll(collisionTree.getPossibleCollidingEntities(getDeactivationRectangle(), true))
        else
            dummyEntitySet.addAll(entityList)
        // update entities using culling
        for (ent in dummyEntitySet) {
            ent.update(delta)

            if (!ent.isStatic && ent.hasMoved()) {
                collisionTree.addDynamicEntityThatMoved(ent)
            }
        }
    }

    /** You may call this to force update of EntityList  */
    open fun removeAndAddEntities() {
        for (ent in toBeRemoved) {
            entityList.removeValue(ent, true)

            if (ent.isStatic) {
                collisionTree.removeStaticEntity(ent)
            } else {
                dynamicEntityList.removeValue(ent, true)
            }

            updateCellsDueToEntity(ent, true)
            ent.justRemovedFromMap()
        }

        for (ent in toBeAdded) {
            entityList.add(ent)

            if (ent.isStatic) {
                collisionTree.addStaticEntity(ent)
            } else {
                dynamicEntityList.add(ent)
            }
            updateCellsDueToEntity(ent, false)
            ent.map = this
            ent.justAddedToMap()
        }

        toBeRemoved.clear()
        toBeAdded.clear()
    }

    private val drawables = Array<DrawableAndSortable>()
    protected val orderComparator = ComparatorByDepthAndYPosition()

    /** Draws map cells and Entities; uses culling on both  */
    override fun draw(gameDrawer: GameDrawer, delta: Float) {
        if (!wasUpdated) {
            com.dcostap.engine.printDebug("WARNING --> draw function is called but the EntityTiledMap wasn't updated!")
            wasUpdated = true // does the warning only one time
        }

        getTilesDrawn()
        getEntitiesDrawn()

        drawables.sort(orderComparator)

        for (drawable in drawables) {
            drawable.draw(gameDrawer, delta)
        }

        // propagate input handling
        inputController.ifNotNull {
            // loop reversed
            for (i in drawables.size - 1 downTo 0) {
                val drawable = drawables.get(i)
                if (drawable.handleTouchInput(it, stageInputController)) break
            }
        }

        drawCellsDebug(gameDrawer, delta)

        if (Engine.DEBUG_COLLISION_TREE_CELLS) {
            collisionTree.debugDrawCellBounds(gameDrawer)

            gameDrawer.alpha = 0.5f
            gameDrawer.color = Color.RED
            gameDrawer.drawRectangle(cameraRectangle, 0.1f, false)
            gameDrawer.color = Color.BLUE
            gameDrawer.drawRectangle(getDeactivationRectangle(), 0.1f, false)
            gameDrawer.resetColor()
            gameDrawer.resetAlpha()
        }

        drawables.clear()
    }

    /** Uses culling */
    private fun getTilesDrawn() {
        val rectangle = Pools.obtain(Rectangle::class.java)
        for (cell in getCellsOccupiedByRectangle(getCameraRectangle(rectangle, drawingCameraBoundsBorder))) {
            drawables.addAll(cell.getTiles())
        }

        Pools.free(rectangle)
    }

    /** Uses culling */
    private fun getEntitiesDrawn() {
        val rectangle = Pools.obtain(Rectangle::class.java)
        for (ent in collisionTree.getPossibleCollidingEntities(getCameraRectangle(rectangle, drawingCameraBoundsBorder),
                true)) {
            drawables.add(ent)
        }

        Pools.free(rectangle)
    }

    protected fun drawCellsDebug(gameDrawer: GameDrawer, delta: Float) {
        if (!Engine.DEBUG || !Engine.DEBUG_MAP_CELLS) return

        val rectangle = Pools.obtain(Rectangle::class.java)
        for (cell in getCellsOccupiedByRectangle(getCameraRectangle(rectangle, drawingCameraBoundsBorder))) {
                if (cell.isSolid) {
                    gameDrawer.alpha = Engine.DEBUG_TRANSPARENCY / 2f
                    gameDrawer.color = Color.BLACK
                    gameDrawer.drawRectangle(cell.x.toFloat(), cell.y.toFloat(), 1f, 1f, 0f, true)
                    gameDrawer.resetColorAndAlpha()
                }

                cell.debugFlashingThing.update(delta)

                if (cell.debugFlashingThing.isFlashing) {
                    cell.debugFlashingThing.setupGameDrawer(gameDrawer)
                    gameDrawer.drawRectangle(cell.x.toFloat(), cell.y.toFloat(), 1f, 1f, 0f, true)
                    cell.debugFlashingThing.resetGameDrawer(gameDrawer)
                }

                gameDrawer.color = Color.BLACK
                gameDrawer.alpha = Engine.DEBUG_TRANSPARENCY / 2f
                gameDrawer.drawRectangle(cell.x.toFloat(), cell.y.toFloat(), 1f, 1f, 0.03f, false)

                gameDrawer.resetColor()
                gameDrawer.resetAlpha()
        }

        Pools.free(rectangle)
    }

    private fun getDeactivationRectangle(): Rectangle {
        getCameraRectangle(deactivationRectangle, entityDeactivationBorder)
        return deactivationRectangle
    }

    /**
     * Returned camera rectangle is updated at the start of the map's update cycle, so be careful about posterior camera
     * modifications (use some border)
     *
     * @param border border around the camera, in game units; can be negative
     */
    fun getCameraRectangle(rectangleToReturn: Rectangle, border: Int): Rectangle {
        rectangleToReturn.set(cameraRectangle)

        if (border == 0) return rectangleToReturn

        rectangleToReturn.x -= border.toFloat()
        rectangleToReturn.y -= border.toFloat()
        rectangleToReturn.width += (border * 2).toFloat()
        rectangleToReturn.height += (border * 2).toFloat()

        return rectangleToReturn
    }

    private fun updateCameraRectangle() {
        val width = worldCamera.viewportWidth * worldCamera.zoom
        val height = worldCamera.viewportHeight * worldCamera.zoom

        cameraRectangle.set(worldCamera.position.x - width / 2,
                worldCamera.position.y - height / 2,
                width, height)
    }

    open fun getClosestValidCell(startingCell: MapCell, allowSolidCells: Boolean, allowCellsWithTiledEntity: Boolean): MapCell? {
        if (isCellValid(startingCell, allowSolidCells, allowCellsWithTiledEntity))
            return startingCell

        var x = 0
        var y = 0
        var amount = 1
        var sign = 1
        var yTurn = false

        // spiral loop around the start cell
        while (true) {
            // if cell is inside map
            val posX: Int = startingCell.x + x
            val posY: Int = startingCell.y + y

            if (isInsideMap(posX.toFloat(), posY.toFloat())) {
                val cell = getMapCell(posX, posY)

                if (isCellValid(startingCell, allowSolidCells, allowCellsWithTiledEntity)) {
                    return cell
                }
            }

            // make a spiral loop
            if (yTurn)
                y += sign
            else
                x += sign

            if (!yTurn && x == sign * amount) {
                yTurn = true
            } else if (yTurn && y == sign * amount) {
                yTurn = false
                sign *= -1

                if (sign == 1)
                    amount++
            }
        }
    }

    open fun isCellValid(mapCell: MapCell, allowSolidCells: Boolean, allowCellsWithTiledEntity: Boolean): Boolean {
        return (allowSolidCells || mapCell.isSolid) && (allowCellsWithTiledEntity || mapCell.staticEntitiesAbove.size == 0)
    }

    fun fixPositionX(x: Int): Int {
        var x2 = x
        x2 = Utils.clamp(x2.toFloat(), 0f, (width - 1).toFloat()).toInt()
        return x2
    }

    fun fixPositionY(y: Int): Int {
        var y2 = y
        y2 = Utils.clamp(y2.toFloat(), 0f, (height - 1).toFloat()).toInt()

        return y2
    }

    fun isInsideMap(x: Float, y: Float): Boolean {
        return x >= 0 && y >= 0 && x < width && y < height
    }

    fun addEntity(ent: Entity) {
        toBeAdded.add(ent)
    }

    fun removeEntity(ent: Entity) {
        if (!ent.isKilled) {
            throw RuntimeException("Tried to remove an Entity directly from map. Use Entity.remove() instead")
        }

        toBeRemoved.add(ent)
    }

    /** Updates cell variables on cells affected by Entity's bounding box  */
    open fun updateCellsDueToEntity(ent: Entity, removeEntity: Boolean) {
        if (!ent.isStatic) return

        for (cell in getCellsOccupiedByRectangle(ent.boundingBox)) {
            // will ignore Static entities that overlap outside the map
            if (ent.getsTiledToCell()) {
                if (removeEntity) {
                    cell.staticEntitiesAbove.removeValue(ent, true)
                } else {
                    cell.staticEntitiesAbove.add(ent)
                }

                if (Engine.DEBUG && Engine.DEBUG_MAP_CELLS)
                    cell.debugFlashingThing.flashColor(Color.YELLOW, 0.6f, 0.1f)

                if (ent.isSolid) {
                    cell.updateHasSolid()

                    if (Engine.DEBUG && Engine.DEBUG_MAP_CELLS)
                        cell.debugFlashingThing.flashColor(Color.RED, 0.6f, 0.1f)
                }
            }
        }
    }

    fun getCellsOccupiedByRectangle(rectangle: Rectangle): Array<MapCell> {
        val cells = dummyCellArray
        cells.clear()

        val rectangleOrigin = Pools.obtain(GridPoint2::class.java)
        val rectangleEnd = Pools.obtain(GridPoint2::class.java)

        rectangleOrigin.set(Math.floor(rectangle.x.toDouble()).toInt(), Math.floor(rectangle.y.toDouble()).toInt())
        rectangleEnd.set(Math.floor((rectangle.x + rectangle.width).toDouble()).toInt(), Math.floor((rectangle.y + rectangle.height).toDouble()).toInt())

        for (xx in rectangleOrigin.x..rectangleEnd.x) {
            for (yy in rectangleOrigin.y..rectangleEnd.y) {
                if (isInsideMap(xx.toFloat(), yy.toFloat()))
                    cells.add(getMapCell(xx, yy))
            }
        }

        Pools.free(rectangleEnd)
        Pools.free(rectangleOrigin)

        return cells
    }

    open fun resetCellTiles() {
        for (cell in getMapCells()) {
            cell.getTiles().clear()
        }
    }

    open fun reset() {
        resetCellTiles()
        entityList.clear()
        toBeAdded.clear()
        toBeRemoved.clear()
    }

    override fun dispose() {

    }

    enum class Event {
        MAP_SOLID_INFORMATION_CHANGED
    }

    /** Saves to a JsonValue: all entities (see [Entity.save]), custom properties, misc. variables (size, deactivation flags), and cells info */
    open fun save(saveEntities: Boolean = true, onlySaveEntitiesProperties: Boolean = false, saveMapProperties: Boolean = true,
             saveMapCells: Boolean = true): JsonValue {
        val info = JsonSavedObject()
        val json = Json()
        info.addChildValue("deactivateEntities", deactivateEntities)
        info.addChildValue("entityDeactivationBorder", entityDeactivationBorder)

        if (saveEntities) {
            removeAndAddEntities()

            info.addChild("entities", JsonSavedObject().also {
                for (ent in entityList) {
                    it.addChild("ent", ent.save(onlySaveEntitiesProperties))
                }
            })
        }

        if (saveMapProperties) info.addChildValue("customProperties", json.toJson(customProperties))

        info.addChildValue("width", width)
        info.addChildValue("height", height)
        info.addChildValue("collisionTree.cellSize", collisionTree.cellSize)

        if (saveMapCells) {
            info.addChildValue("cells", JsonSavedObject().also {
                for (cell in mapCells.values()) {
                    it.addChild("cell", cell.save())
                }
            })
        }

        return info
    }

    /** Will ignore entities, custom properties, and cells whenever any of those is not saved (so basically *only* loads what is saved) */
    open fun load(savedMapInfoJson: JsonValue, entityLoaderFromClass: EntityLoaderFromClass? = null) {
        val json = Json()

        initMap(savedMapInfoJson.getInt("width", 0),
                savedMapInfoJson.getInt("height", 0),
                savedMapInfoJson.getInt("collisionTree.cellSize", 0))

        val ents = savedMapInfoJson.get("entities")
        if (ents != null) {
            for (entInfo in ents) {
                if (entityLoaderFromClass != null) {
                    val clazz = Class.forName(entInfo.getString("class")) as Class<Entity>
                    var ent: Entity? = entityLoaderFromClass.loadEntity(clazz)
                    if (ent == null) ent = clazz.newInstance()
                    addEntity(ent!!)
                    ent.load(entInfo)
                }
            }
        }

        val cells = savedMapInfoJson.get("cells")
        if (cells != null) {
            for (cellInfo in cells) {
                mapCells.put(GridPoint2(cellInfo.getInt("x"), cellInfo.getInt("y")), MapCell.load(cellInfo, this, engine.getAssets()))
            }
        }

        customProperties = json.fromJson(CustomProperties::class.java, savedMapInfoJson.getString("customProperties")) ?: customProperties
    }
}


/**
 * Created by Darius on 28/10/2017.
 *
 * Orders entities based on depth, and then y-position if depth is equal. Higher depth means further away from camera.
 * Entity with -10 depth is drawn above than another with 30 depth
 */
class ComparatorByDepthAndYPosition : Comparator<DrawableAndSortable> {
    override fun compare(o1: DrawableAndSortable, o2: DrawableAndSortable): Int {
        val depth = o2.getDrawingRepresentativeDepth().compareTo(o1.getDrawingRepresentativeDepth())

        if (depth == 0) return o2.getDrawingRepresentativeY().compareTo(o1.getDrawingRepresentativeY())
        else return depth
    }
}