package com.dcostap.engine.map

import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.utils.JsonValue
import com.dcostap.engine.Assets
import com.dcostap.engine.utils.GameDrawer
import com.dcostap.engine.utils.JsonSavedObject
import com.dcostap.engine.utils.Saveable
import com.dcostap.engine.utils.addChildValue
import com.dcostap.engine.utils.input.InputController

/**
 * Created by Darius on 11/01/2018
 */
open class Tile(var depth: Int, var tileSpr: TextureRegion) : DrawableAndSortable, Saveable {
    var rotation: Int = 0
    lateinit var cell: MapCell

    /** (optional) So that the texture can be loaded / saved */
    var tileSprName: String? = null

    override fun draw(gameDrawer: GameDrawer, delta: Float) {
        gameDrawer.draw(tileSpr, cell.x.toFloat(), cell.y.toFloat(), rotation.toFloat())
    }

    override fun getDrawingRepresentativeY(): Float {
        return cell.y.toFloat()
    }

    override fun getDrawingRepresentativeDepth(): Int {
        return depth
    }

    override fun save(): JsonValue {
        if (tileSprName.isNullOrEmpty()) {
            com.dcostap.engine.printDebug("WARNING: Tried to save a Tile which has a texture assigned but has no name saved in the corresponding " +
                    "variable. Thus the texture won't be loaded correctly. To solve it save the texture name when setting the texture")
        }

        val json = JsonSavedObject()
        json.addChildValue("rotation", rotation)
        json.addChildValue("depth", depth)
        json.addChildValue("tileSprName", if (tileSprName == null) "null" else tileSprName!!)
        return json
    }

    companion object {
        fun load(json: JsonValue, assets: Assets): Tile {
            if (json.getString("tileSprName") != "null") {
                val tileSprName = json.getString("tileSprName")
                val tile = Tile(json.getInt("depth"), assets.findRegionFromRawImageName(tileSprName!!))
                tile.rotation = json.getInt("rotation")

                tile.tileSprName = tileSprName
                return tile
            }

            throw RuntimeException("couldn't load tile cause the sprName wasn't saved")
        }
    }

    override fun handleTouchInput(inputController: InputController, stageInputController: InputController?): Boolean {
        return false
    }
}
