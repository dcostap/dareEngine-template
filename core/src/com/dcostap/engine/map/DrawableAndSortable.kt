package com.dcostap.engine.map

import com.dcostap.engine.utils.Drawable
import com.dcostap.engine.utils.input.InputController

/** Created by Darius on 21-Aug-18.
 *
 * Something that can be sorted to specify drawing order. Depth is the most important. If 2 things have same depth then y is compared */
open interface DrawableAndSortable : Drawable {
    /** Higher depth = further away from the camera */
    fun getDrawingRepresentativeDepth(): Int

    /** Used to compare when depth is equal */
    fun getDrawingRepresentativeY(): Float

    /** Called on all visible Drawable after drawing in a [EntityTiledMap]
     * Use this for correct input handling on a EntityTiledMap: since it happens after drawing the call order will
     * correspond to the sorting order. So something that is drawn above something else will receive the input first.
     *
     * @return handle the input and avoid any other Drawable to receive the function */
    fun handleTouchInput(inputController: InputController, stageInputController: InputController?): Boolean
}
